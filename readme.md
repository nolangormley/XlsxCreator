# FilePro Excel File Creator

This program creates an excel file from scratch using the open xml format and xlsx file format. The only third party resource this program relies on is Miniz. This is packaged with the project and will not require installation.

# Developing on this project

This program contains Visual Studio files and works on Visual Studio and \*nix. The only thing that will need changed when you clone this project is the include directories. You will need to match them to your project path.

# Help and Documentation

## Quirks

The sharedStrings is a shared array of the workbook. When you initilize a new worksheet, it must take a pointer to the workbook's sharedStrings array to work correctly with multiple worksheets.