#ifndef __B_TREE_H__
#define __B_TREE_H__

#include <stdint.h>
#include <sys/types.h>

typedef struct btree_s {
	uintptr_t _id;
	RECNO _rec;
	struct btree_t *_right;
	struct btree_t *_left;
} BTREE, *PBTREE, **PPBTREE;

void insert(PPBTREE tree, uintptr_t id, RECNO recno);
void deltree(PBTREE tree);
void inorder_action(PPBTREE tree, void (*func)(PBTREE));
PBTREE getmin(btree_t *node);
PBTREE delnode(PPBTREE tree, uintptr_t id);
PBTREE search(PPBTREE tree, uintptr_t id);

#endif
