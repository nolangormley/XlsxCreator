#ifndef _FPXLSXCREATOR_H
#define _FPXLSXCREATOR_H

#ifndef FP_EXPORT
#	if defined(_WIN32) || defined(_WIN64)
#		if defined(__SHARED_OBJ__)
#			define FP_EXPORT extern __declspec(dllexport)
#		else
#			define FP_EXPORT extern __declspec(dllimport)
#		endif
#	else
#		define FP_EXPORT extern
#	endif
#endif	/* !FP_EXPORT */

#include <Workbook.h>
#include <Chart.h>
#include <Worksheet.h>
#include <PathManager.h>
#include <vector.h>

/// Struct to make using this library easier
typedef struct XLSXMain_s {
	PPATH_MANAGER pm;
	PWORKSHEET mainsheet;
	PWORKBOOK book;
} XLSXMAIN, *PXLSXMAIN;

/// Initializes the main struct's Path Manager and Workbook
FP_EXPORT PXLSXMAIN initXlsx(char * tmppath, char * filename, char * sheetname);

/// Adds a cell data pointer to a given vector
FP_EXPORT void addCellData(PVECTOR vec, void * value, size_t type, size_t style);

/// Adds a vector of cell data pointers to a sheet, creating a while row of data
FP_EXPORT void addRowData(PWORKSHEET sheet, PVECTOR vec);

/// Adds a bar chart with data from the given coordinates (1 based indexing)
FP_EXPORT int createBarChart(PXLSXMAIN main, char * title, PCELL_COORD valFrom, PCELL_COORD valTo);

/// Adds a linear chart with data from the given coordinates (1 based indexing)
FP_EXPORT int createLinearChart(PXLSXMAIN main, char * title, PCELL_COORD valFrom, PCELL_COORD valTo);

/// Adds a scatter chart with data from the given coordinates (1 based indexing)
FP_EXPORT int createScatterChart(PXLSXMAIN main, char * title, PCELL_COORD valFrom, PCELL_COORD valTo, PCELL_COORD catFrom, PCELL_COORD catTo);

/// Saves all xml and creates the xlsx file
FP_EXPORT int save(PXLSXMAIN main);

#endif // !FPXLSXCREATOR_H
