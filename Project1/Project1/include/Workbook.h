#ifndef _WORKBOOK_H
#define _WORKBOOK_H

#include <xlsxDef.h>
#include <zip.h>

//#include <PathManager.h>
#include <Chartsheet.h>
//#include <Worksheet.h>
#include <StyleList.h>

typedef struct workbook_s {
	char*                   temp_path;		///< path to the temporary directory (unique for a book)
	PVECTOR                 worksheets;		///< a series of data sheets
	PVECTOR	                chartsheets;	///< a series of chart sheets
	PVECTOR                 charts;         ///< a series of charts
	PVECTOR                 drawings;       ///< a series of drawings
	PVECTOR					sharedStrings;  ///< a series of strings

	struct zip_t	*		zip;
	int						commLastId;		///< m_commLastId comments counter
	char*					UserName;
	int						sheetId;         ///< Current sheet sequence number (for sheets ordering)
	int						activeSheetIndex;///< Index of active (opened) sheet

	PSTYLE_LIST				styleList;       ///< All registered styles

	PPATH_MANAGER			pathManager;     ///<
} WORKBOOK, *PWORKBOOK;

void wrkbk_init(PWORKBOOK *book, PPATH_MANAGER pm, char* path, char * filename);
int wrkbk_save(PWORKBOOK book);
void wrkbk_saveCore(PWORKBOOK book);
void wrkbk_saveContentType(PWORKBOOK book);
void wrkbk_saveApp(PWORKBOOK book);
void wrkbk_saveTheme(PWORKBOOK book);
void wrkbk_saveStyles(PWORKBOOK book);
void wrkbk_saveSharedStrings(PWORKBOOK book);
void wrkbk_saveWorkbook(PWORKBOOK book);

PWORKSHEET InitWorkSheet(PWORKBOOK book, PWORKSHEET sheet, char* title);

void AddNumberFormats(PWORKBOOK book, PXML_WRITER xml);
void GetFormatCodeString(char retValBuf[], PWORKBOOK book, PNUM_FORMAT fmt);
void GetFormatCodeColor(char retValBuf[], PWORKBOOK book, ENumericStyleColor color);
void AddFonts(PWORKBOOK book, PXML_WRITER xml);
void AddFontInfo(PXML_WRITER xml, PFONT font, char * FontTagName, int Charset);

int zip_addFile(struct zip_t * zip, char * path, char * name);

PWORKSHEET wrkbk_addSheet(PWORKBOOK book, char * title);
PCHART wrkbk_addChart(PWORKBOOK book, PWORKSHEET sheet, DRAWINGPOINT TopLeft, DRAWINGPOINT BottomRight, EChartTypes type);
PCHARTSHEET wrkbk_addChartSheet(PWORKBOOK book, const char * title, EChartTypes type);
DRAWING * wrkbk_createDrawing(PWORKBOOK book);

#endif
