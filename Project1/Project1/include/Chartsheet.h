#ifndef _CHARTSHEET_H
#define _CHARTSHEET_H

#include <stdlib.h>
#include <Chart.h>
#include <Drawing.h>

typedef struct chartsheet_s {
	size_t index;            ///< chart ID number
	PCHART chart;            ///< Reference to chart
	PDRAWING drawing;          ///< Reference to drawing object
	PPATH_MANAGER pathManager;      ///< reference to XML PathManager
} CHARTSHEET, *PCHARTSHEET;

PCHARTSHEET crtsht_init(PCHARTSHEET chartsht, size_t index, PCHART chart,  DRAWING * drawing, PPATH_MANAGER pathmanager);
int crtsht_save(PCHARTSHEET sheet);

#endif
