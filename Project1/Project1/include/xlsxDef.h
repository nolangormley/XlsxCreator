#ifndef __XLSX_DEF_H__
#define __XLSX_DEF_H__

#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#define PATH_MAX	512

// **********************************    Enums   **********************************
// Chart types
typedef enum EChartTypes {
	CHART_NONE 		= -1,
	CHART_LINEAR 	= 0,
	CHART_BAR,
	CHART_SCATTER
} EChartTypes;

// Border attributes
typedef enum EBorderStyle {
	BORDER_NONE = 0,
	BORDER_THIN,
	BORDER_MEDIUM,
	BORDER_DASHED,
	BORDER_DOTTED,
	BORDER_THICK,
	BORDER_DOUBLE,
	BORDER_HAIR,
	BORDER_MEDIUM_DASHED,
	BORDER_DASH_DOT,
	BORDER_MEDIUM_DASH_DOT,
	BORDER_DASH_DOT_DOT,
	BORDER_MEDIUM_DASH_DOT_DOT,
	BORDER_SLANT_DASH_DOT
} EBorderStyle;

// Font attributes
typedef enum EFontAttributes {
	FONT_NORMAL     = 0,
	FONT_BOLD       = 1,
	FONT_ITALIC     = 2,
	FONT_UNDERLINED = 4,
	FONT_STRIKE     = 8,
	FONT_OUTLINE    = 16,
	FONT_SHADOW     = 32,
	FONT_CONDENSE   = 64,
	FONT_EXTEND     = 128
} EFontAttributes;

// Fill`s pattern type
typedef enum EPatternType {
	PATTERN_NONE = 0,
	PATTERN_SOLID,
	PATTERN_MEDIUM_GRAY,
	PATTERN_DARK_GRAY,
	PATTERN_LIGHT_GRAY,
	PATTERN_DARK_HORIZ,
	PATTERN_DARK_VERT,
	PATTERN_DARK_DOWN,
	PATTERN_DARK_UP,
	PATTERN_DARK_GRID,
	PATTERN_DARK_TRELLIS,
	PATTERN_LIGHT_HORIZ,
	PATTERN_LIGHT_VERT,
	PATTERN_LIGHT_DOWN,
	PATTERN_LIGHT_UP,
	PATTERN_LIGHT_GRID,
	PATTERN_LIGHT_TRELLIS,
	PATTERN_GRAY_125,
	PATTERN_GRAY_0625
} EPatternType;

// Text horizontal alignment
typedef enum EAlignHoriz {
	ALIGN_H_NONE = 0,
	ALIGN_H_LEFT,
	ALIGN_H_CENTER,
	ALIGN_H_RIGHT
} EAlignHoriz;

// Text vertical alignment
typedef enum EAlignVert {
	ALIGN_V_NONE = 0,
	ALIGN_V_TOP,
	ALIGN_V_CENTER,
	ALIGN_V_BOTTOM
} EAlignVert;

// Number styling
typedef enum ENumericStyle {
	NUMSTYLE_GENERAL = 0,
	NUMSTYLE_NUMERIC,
	NUMSTYLE_PERCENTAGE,
	NUMSTYLE_EXPONENTIAL,
	NUMSTYLE_FINANCIAL,
	NUMSTYLE_MONEY,
	NUMSTYLE_DATE,
	NUMSTYLE_TIME,
	NUMSTYLE_DATETIME
} ENumericStyle;

// Number can be colored differently depending on whether it positive or negavite or zero
typedef enum ENumericStyleColor {
	NUMSTYLE_COLOR_DEFAULT = 0,
	NUMSTYLE_COLOR_BLACK,
	NUMSTYLE_COLOR_GREEN,
	NUMSTYLE_COLOR_WHITE,
	NUMSTYLE_COLOR_BLUE,
	NUMSTYLE_COLOR_MAGENTA,
	NUMSTYLE_COLOR_YELLOW,
	NUMSTYLE_COLOR_CYAN,
	NUMSTYLE_COLOR_RED
} ENumericStyleColor;

// Can add more types if necessary
typedef enum ECellDataType {
	CELLTYPE_INT = 0,
	CELLTYPE_STR
} ECellDataType;
 

// ********************************** Structures **********************************

typedef struct drawingPoint_s {
	uint32_t col;	///< Column (starts from 0)
	uint32_t colOff;///< Column Offset (starts from 0)
	uint32_t row;	///< Row (starts from 0)
	uint32_t rowOff;///< Row Offset (starts from 0)
} DRAWINGPOINT, *PDRAWINGPOINT;

#define font_eq(f1, f2) (strcmp(f1.name, f2.name) == 0 && strcmp(f1.color, f2.color) == 0 && f1.size == f2.size && f1.attributes == f2.attributes && f1.theme == f2.theme)

typedef struct font_s {
	char 	name[64];
	char 	color[8];
	int32_t size;
	int32_t attributes;
	int 	theme;
	
} FONT, *PFONT;

#define fill_eq(f1, f2) (f1.patternType == f2.patternType && strcmp(f1.fgColor, f2.fgColor) == 0 && strcmp(f1.bgColor, f2.bgColor) == 0)

typedef struct fill_s {
	EPatternType patternType;
	char 		 fgColor[8];
	char 		 bgColor[8];
	
} FILL, *PFILL;

#define border_item_eq(b1, b2) (b1.style == b2.style && strcmp(b1.color, b2.color) == 0)

typedef struct border_item_s {
	EBorderStyle style;
	char 		 color[8];
	
} BORDER_ITEM, *PBORDER_ITEM;

 typedef struct border_s {
	BORDER_ITEM left;
	BORDER_ITEM right;
	BORDER_ITEM bottom;
	BORDER_ITEM top;
	BORDER_ITEM diagonal;
	int 		isDiagonalUp;
	int 		isDiagonalDown;
	
 } BORDER, *PBORDER;

 #define num_format_eq(n1, n2) 	(n1.id == n2.id && n1.numberOfDigitsAfterPoint == n2.numberOfDigitsAfterPoint && \
								n1.showThousandsSeparator == n2.showThousandsSeparator && strcmp(n1.format, n2.format) == 0 && \
								n1.numberStyle == n2.numberStyle && n1.positiveColor == n2.positiveColor && n1.negativeColor == n2.negativeColor && \
								n1.zeroColor == n2.zeroColor)
 
 // ID MUST BE 164
 typedef struct number_format_s {
	size_t 				id;
	size_t 				numberOfDigitsAfterPoint;
	int 				showThousandsSeparator;
	char 				*format;
	ENumericStyle 		numberStyle;
	ENumericStyleColor 	positiveColor;
	ENumericStyleColor 	negativeColor;
	ENumericStyleColor 	zeroColor;

 } NUM_FORMAT, *PNUM_FORMAT;
 
 typedef struct style_s {
	FONT 		font;
	FILL 		fill;
	BORDER 		border;
	NUM_FORMAT 	numFormat;
	EAlignHoriz horizAlign;
	EAlignVert 	vertAlign;
	int 		wrapText;
	
 } STYLE, *PSTYLE;
 
 typedef struct cell_coord_s {
	uint32_t row;	// row (starts from 1)
	uint32_t col;	// col (starts from 0)
	
 } CELL_COORD, *PCELL_COORD;
 
 typedef struct column_width_s {
	uint32_t colFrom;	// column range from (starts from 0)
	uint32_t colTo;		// column range to (starts from 0)
	float 	 width;
			
 } COLUMN_WIDTH, *PCOLUMN_WIDTH;
 
 typedef struct comment_s {
	size_t 		index;
	// list (pair of font and string)
	CELL_COORD 	cellRef;
	char 		fillColor[8];
	int			hidden;
	int			x;
	int			y;
	int			width;
	int			height;
	
} COMMENT, *PCOMMENT;

 typedef struct cell_data_void {
	 void	*value;
	 size_t	style;
	 size_t type;
 } CELL_DATA_VOID, *PCELL_DATA_VOID;

typedef struct cell_data_str_s {
	char 	*value;
	size_t 	style;
} CELL_DATA_STR, *PCELL_DATA_STR;

typedef struct cell_data_time_s {
	time_t 	value;
	size_t 	style;
} CELL_DATA_TIME, *PCELL_DATA_TIME;

typedef struct cell_data_int_s {
	int32_t	value;
	size_t 	style;
} CELL_DATA_INT, *PCELL_DATA_INT;

typedef struct cell_data_uint_s {
	uint32_t value;
	size_t 	 style;
} CELL_DATA_UINT, *PCELL_DATA_UINT;

typedef struct cell_data_dbl_s {
	double 	value;
	size_t 	style;
} CELL_DATA_DBL, *PCELL_DATA_DBL;

typedef struct cell_data_flt_s {
	float 	value;
	size_t 	style;
} CELL_DATA_FLT, *PCELL_DATA_FLT;
 
 
// **********************************   Methods  **********************************
//I haven't the slighest clue if this will write past the buffer
void cell_coord_toString(char retValBuf[], PCELL_COORD cell);

#endif
