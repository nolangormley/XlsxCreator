/*
	No functions within chart besides init and save should be called externally. All of the rest are called from within save
*/

#ifndef _CHART_H_
#define _CHART_H_

#include <Worksheet.h>
#include <time.h>
#include <vector.h>

typedef enum EPosition {
	POS_NONE = 0,
	POS_LEFT,
	POS_RIGHT,
	POS_TOP,
	POS_BOTTOM,
	POS_LEFT_ASIDE,
	POS_RIGHT_ASIDE,
	POS_TOP_ASIDE,
	POS_BOTTOM_ASIDE
} EPosition;

/// @brief  Axis grid detalization property
typedef enum EGridLines {
	GRID_NONE = 0,
	GRID_MAJOR,
	GRID_MINOR,
	GRID_MAJOR_N_MINOR
} EGridLines;

/// @brief  The enumeration contains values that determine diagramm diagram data table property
typedef enum ETableData {
	TBL_DATA_NONE = 0,
	TBL_DATA,
	TBL_DATA_N_KEYS
} ETableData;

/// @brief  The enumeration contains values that demermine point where axis will pass through
typedef enum EAxisCross {
	CROSS_AUTO_ZERO = 0,
	CROSS_MIN,
	CROSS_MAX
} EAxisCross;

/// @brief	The enumeration determines bars` direction in bar diagramm
typedef enum EBarDirection {
	BAR_DIR_VERTICAL = 0,
	BAR_DIR_HORIZONTAL
} EBarDirection;

/// @brief	The enumeration determinese several bar relative position
typedef enum EBarGrouping {
	BAR_GROUP_CLUSTERED = 0,
	BAR_GROUP_STACKED,
	BAR_GROUP_PERCENT_STACKED
} EBarGrouping;

/// @brief	The enumeration determines scatter diagram styles
typedef enum EScatterStyle {
	SCATTER_FILL = 0,
	SCATTER_POINT
} EScatterStyle;
typedef enum joinType { joinNone, joinLine, joinSmooth } joinType;
typedef enum dashType { dashSolid, dashDot, dashShortDash, dashDash } dashType;
typedef enum symType { symNone, symDiamond, symCircle, symSquare, symTriangle } symType;

// This struct was only inside the Series struct. Not really sure what it is used for yet
typedef struct stMarker_s {
	//Originally in series, but had to be moved for marker. Replace to global?

	symType Type;			     				///< SymType indicates whether and how nodes are marked
	size_t Size;								///< SymSize 1 ... 10(?) was originally a string
	char * LineColor;				            ///< Color RGB string like "FF00FF"
	char * FillColor;				            ///< Color RGB string like "FF00FF"
	double LineWidth;							///< Like in excell 0.5 ... 3.0(?)
	int initialized;

} MARKER, *PMARKER;

typedef struct series_s {

	PWORKSHEET catSheet;							///< catSheet pointer to a sheet contains category axis
	PCELL_COORD catAxisFrom;							///< catAxisFrom data range (from cell (row, col), beginning since 0)
	PCELL_COORD catAxisTo;							///< catAxisTo data range (to cell (row, col), beginning since 0)
	PWORKSHEET valSheet;							///< valSheet pointer to a sheet contains values axis
	PCELL_COORD valAxisFrom;							///< valAxisFrom data range (from cell (row, col), beginning since 0)
	PCELL_COORD valAxisTo;							///< valAxisTo data range (to cell (row, col), beginning since 0)

	int initialized;
	char * title;									///< title series title (in the legend) was originally a _tstring

	joinType JoinType;						     	///< JoinType indicates whether series must be joined and smoothed at rendering
	double LineWidth;                               ///< Like in excell  0.5 ... 3.0 (?)
	char * LineColor;                               ///< LineGolor RGB string like "FF00FF" was originally a string
	dashType DashType;							    ///< DashType indicates whether line will be rendered dashed or not

	PMARKER Marker;
} SERIES, *PSERIES;

typedef struct axis_s {
	uint32_t id;			///< axis axis id
	char * name;			///< name axis name (that will be depicted) changed from _tstring
	uint32_t nameSize;		///< nameSize font size
	EPosition pos;			///< pos axis position
	EGridLines gridLines;	///< gridLines grid detalization
	EAxisCross cross;		///< cross determines axis position relative to null
	int sourceLinked;		///< sourceLinked indicates if axis has at least one linked source. bool
	char * minValue;		///< minValue minimum value for axis (type string is used for generality and processing simplicity)
	char * maxValue;		///< maxValue minimum value for axis (type string is used for generality and processing simplicity)
	int lblSkipInterval;	///< space between two neighbour labels
	int markSkipInterval;	///< space between two neighbour marks
	int lblAngle;			///< axis labels angle in degrees
	int isVal;              ///< "Val" axis is normal for scatter and line plots. bool
	int initialized;

} AXIS, *PAXIS;

typedef struct diagram_s {
	char * name;				///< name diagramm name (that will be depicted above the chart) changed from _tstring
	uint32_t nameSize;			///< nameSize font size
	EPosition legend_pos;		///< legend_pos legend position
	ETableData tableData;		///< tableData table data state
	EChartTypes typeMain;		///< typeMain main chart type
	EChartTypes typeAdditional;	///< typeAdditional additional chart type (optional)

	int initialized;
	EBarDirection barDir;
	EBarGrouping barGroup;
	EScatterStyle scatterStyle;

} DIAGRAM, *PDIAGRAM;

typedef struct chart_s {
	PXML_WRITER xmlWriter;
	PDIAGRAM diagram;
	size_t index;            ///< chart ID number
	PVECTOR seriesSet;        ///< series set for main chart type
	PVECTOR seriesSetAdd;     ///< series set for additional chart type
	PAXIS xAxis;            ///< main X axis object
	PAXIS yAxis;            ///< main Y axis object
	PAXIS x2Axis;           ///< additional X axis object
	PAXIS y2Axis;           ///< additional Y axis object

	int initialized;
	char * title;            ///< chart sheet title

	PPATH_MANAGER pathManager;
} CHART, *PCHART;

void crt_free(PCHART chart);
void crt_freeSeries(PSERIES series);

PCHART crt_init(PCHART chart, size_t index, EChartTypes type, PPATH_MANAGER manager);
PAXIS crt_axisInit(PAXIS axis);
PSERIES crt_seriesInit(PSERIES series);
PCELL_COORD crt_cell_coordInit(int row, int col);

int crt_save(PCHART chart);

void crt_addTitle(PCHART chart, const char * name, int size, int vertPos);
void crt_addTableData(PCHART chart);
void crt_addLegend(PCHART chart);
void crt_addXAxis(PCHART chart, PAXIS axis, uint32_t crossAxisId);
void crt_addYAxis(PCHART chart, PAXIS axis, uint32_t crossAxisId);

void crt_addLineChart(PCHART chart, PAXIS axis, uint32_t yAxisId, PVECTOR seriesVec, size_t firstSeriesId);
void crt_addBarChart(PCHART chart, PAXIS xAxis, uint32_t yAxisId, PVECTOR seriesVec, size_t firstSeriesId, EBarDirection barDir, EBarGrouping barGroup);
void crt_addScatterChart(PCHART chart, uint32_t xAxisId, uint32_t yAxisId, PVECTOR seriesVec, size_t firstSeriesId, EScatterStyle style);

int crt_addSeries(PCHART chart,  PSERIES series, int mainChart);
void GetCharForPos(char retValBuf[], EPosition Pos, char* DefaultChar);
void crt_cellRangeString(char retValBuf[], char* title, PCELL_COORD CellFrom, PCELL_COORD CellTo);

#endif
