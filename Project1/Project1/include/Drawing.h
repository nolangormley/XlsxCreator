#ifndef _DRAWING_H
#define _DRAWING_H

#include <xlsxDef.h>
#include <PathManager.h>
#include <vector.h>
#include <Chart.h>

typedef enum AnchorType {
	absoluteAnchor,         //For CChartSheet
	twoCellAnchor,          //For chart on CWorkSheet
	imageOneCellAnchor,     //For image on CWorkSheet
	imageTwoCellAnchor     //For image on CWorkSheet
} ANCHORTYPE;

typedef struct Drawing_s {
	size_t           index;            ///< Drawing ID number
	PPATH_MANAGER    pathManager;      ///< reference to XML PathManager
	PVECTOR			 drawings;         ///< Vector of Drawing Info
} DRAWING, *PDRAWING;

typedef struct DrawingInfo_s {
	PCHART Chart;
	ANCHORTYPE  AType;
	DRAWINGPOINT  TopLeft;        //For drawing on CWorkSheet
	DRAWINGPOINT  BottomRight;    //For drawing on CWorkSheet
} DRAWINGINFO, *PDRAWINGINFO;


/// Function headers

PDRAWING dr_init(DRAWING * draw, size_t index, PPATH_MANAGER pm);
int dr_save(DRAWING * draw);
int dr_saveDrawingRels(DRAWING * draw);
int dr_saveDrawing(DRAWING * draw);
void dr_saveChartSection(PXML_WRITER xml, PCHART chart, int rId);
void dr_saveChartPoint(PXML_WRITER xml, const char * Tag, const DRAWINGPOINT Point);
void dr_appendChart(DRAWING * drawing, PCHART chart, DRAWINGPOINT bottomright, DRAWINGPOINT topleft);

#endif // !_DRAWING_H
