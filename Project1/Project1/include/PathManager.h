#ifndef __PATH_MANAGER_H__
#define __PATH_MANAGER_H__

#include <xlsxDef.h>
#include <vector.h>

typedef struct path_manager_s {
	char tempPath[PATH_MAX];
	PVECTOR tempDirs;
	PVECTOR contentFiles;
	
} PATH_MANAGER, *PPATH_MANAGER;


PPATH_MANAGER pm_init(PPATH_MANAGER manager, const char *path);
void pm_free(PPATH_MANAGER manager);

int makeDirectory(PPATH_MANAGER manager, const char *path);
char *registerFile(PPATH_MANAGER manager, const char *path);

void pm_clearTemp(PPATH_MANAGER manager);


#endif
