#ifndef __VECTOR_H__
#define __VECTOR_H__

#include <stdlib.h>
#include <string.h>

#define INIT_CAP	256

typedef struct vector_s {
	int 	top;
	size_t 	capacity;
	void 	**array;
} VECTOR, *PVECTOR;

PVECTOR vec_init();
int  	vec_full(PVECTOR vec);
int  	vec_empty(PVECTOR vec);
void 	*vec_remove(PVECTOR vec, int index);
void 	vec_pushback(PVECTOR vec, void *data);
void 	vec_pushfront(PVECTOR vec, void *data);
void	vec_free(PVECTOR vec);
void	vec_grow(PVECTOR vec);

#endif
