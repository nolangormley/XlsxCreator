#ifndef __STYLE_LIST_H__
#define __STYLE_LIST_H__

#include <xlsxDef.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>

#define BUILT_IN_STYLES_NUMBER 	164
#define STYLE_LINK_NUMBER 		4

typedef enum EStyleLink {
	STYLE_LINK_BORDER = 0,
	STYLE_LINK_FONT,
	STYLE_LINK_FILL,
	STYLE_LINK_NUM_FORMAT
} EStyleLink;

typedef struct triplet_s {
	EAlignHoriz t1;
	EAlignVert	t2;
	int			t3;
} TRIPLET, *PTRIPLET;

typedef struct style_list_s {
	size_t fmtLastId;

	PVECTOR borders;
	PVECTOR fonts;
	PVECTOR fills;
	PVECTOR nums;
	PVECTOR styleIndexes;
	PVECTOR stylePos;

} STYLE_LIST, *PSTYLE_LIST;

PSTYLE_LIST sl_init(PSTYLE_LIST stylelist);
void sl_free(PSTYLE_LIST stylelist);

size_t sl_add(PSTYLE_LIST stylelist, PSTYLE style);

#endif
