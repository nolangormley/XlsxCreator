#ifndef __WORKSHEET_H__
#define __WORKSHEET_H__

#include <xlsxDef.h>
#include <xmlWriter.h>
#include <PathManager.h>
#include <time.h>
#include <vector.h>

typedef enum EPageOrientation {
	PAGE_PORTRAIT = 0,
	PAGE_LANDSCAPE
} EPageOrientation;

typedef struct worksheet_s {
	PXML_WRITER xmlWriter;
	PVECTOR 	calcChain;
	PVECTOR 	comments;
	PVECTOR 	mergedCells;
	PVECTOR  	sharedStrings;

	size_t 		index;
	char 		*title;
	int 		withFormula;
	int 		withComments;
	int 		initialized;
	int 		isDataPresented;
	int 		row_opened;
	uint32_t 	row_index;
	uint32_t 	cur_column;
	uint32_t 	offset_column;

	EPageOrientation orientation;

	PPATH_MANAGER pathManager;
	struct DRAWING * drawing;

} WORKSHEET, *PWORKSHEET;

PWORKSHEET ws_init(PWORKSHEET worksheet, struct DRAWING * drawing, PPATH_MANAGER manager, size_t index, uint32_t frozenWidth, uint32_t frozenHeight, PVECTOR colWidths);
void ws_free(PWORKSHEET worksheet);

void ws_addRow(PWORKSHEET sheet, PVECTOR data, uint32_t offset, uint32_t height);
void ws_beginRow(PWORKSHEET worksheet, uint32_t height);
void ws_endRow(PWORKSHEET worksheet);

void ws_addCells(PWORKSHEET worksheet, PVECTOR cells);
void ws_addCellInlnStr(PWORKSHEET worksheet, PCELL_DATA_STR data);
void ws_addCellStr(PWORKSHEET worksheet, PCELL_DATA_STR data);
void ws_addCellTime(PWORKSHEET worksheet, PCELL_DATA_TIME data);
void ws_addCellInt(PWORKSHEET worksheet, PCELL_DATA_INT data);
void ws_addCellUInt(PWORKSHEET worksheet, PCELL_DATA_UINT data);
void ws_addCellDbl(PWORKSHEET worksheet, PCELL_DATA_DBL data);
void ws_addCellFlt(PWORKSHEET worksheet, PCELL_DATA_FLT data);

void ws_addCellInlnStrR(PWORKSHEET worksheet, const char *value, size_t style);
void ws_addCellStrR(PWORKSHEET worksheet, const char *value, size_t style);
void ws_addCellTimeR(PWORKSHEET worksheet, time_t value, size_t style);
void ws_addCellIntR(PWORKSHEET worksheet, int32_t value, size_t style);
void ws_addCellUIntR(PWORKSHEET worksheet, uint32_t value, size_t style);
void ws_addCellDblR(PWORKSHEET worksheet, double value, size_t style);
void ws_addCellFltR(PWORKSHEET worksheet, float value, size_t style);

void ws_addRowStr(PWORKSHEET worksheet, PCELL_DATA_STR *data, size_t count, uint32_t offset, uint32_t height);
void ws_addRowTime(PWORKSHEET worksheet, PCELL_DATA_TIME *data, size_t count, uint32_t offset, uint32_t height);
void ws_addRowInt(PWORKSHEET worksheet, PCELL_DATA_INT *data, size_t count, uint32_t offset, uint32_t height);
void ws_addRowUInt(PWORKSHEET worksheet, PCELL_DATA_UINT *data, size_t count, uint32_t offset, uint32_t height);
void ws_addRowDbl(PWORKSHEET worksheet, PCELL_DATA_DBL *data, size_t count, uint32_t offset, uint32_t height);
void ws_addRowFlt(PWORKSHEET worksheet, PCELL_DATA_FLT *data, size_t count, uint32_t offset, uint32_t height);

void ws_mergeCells(PWORKSHEET worksheet, CELL_COORD from, CELL_COORD to);

void ws_addFrozenPane(PWORKSHEET worksheet, uint32_t frozenWidth, uint32_t frozenHeight);

int ws_save(PWORKSHEET worksheet);
int ws_saveSheetRels(PWORKSHEET worksheet);

#endif
