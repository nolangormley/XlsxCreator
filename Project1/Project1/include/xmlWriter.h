#ifndef __XML_WRITER_H__
#define __XML_WRITER_H__

#include <xlsxDef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <assert.h>

#define STACK_DEFAULT 	256

// Structures
typedef struct stack_s {
	int top;
	unsigned int capacity;
	char **array;
	
} STACK, *PSTACK;

typedef struct xml_writer_s {
	int 	tag_open;
	int 	self_closed;
	FILE 	*fstream;
	PSTACK 	tags;
	
} XML_WRITER, *PXML_WRITER;

// Methods

void xlsx_initWriter(PXML_WRITER * writer, const char *fileName);
void xlsx_freeWriter(PXML_WRITER * writer);

void xlsx_closeOpenTag(PXML_WRITER writer);
void xlsx_writeStringEscaped(PXML_WRITER writer, const char *str);
void xlsx_tag(PXML_WRITER writer, const char *tag);
void xlsx_tagL(PXML_WRITER writer, const char *tag);
void xlsx_endL(PXML_WRITER writer);
void xlsx_end(PXML_WRITER writer, const char *tag);
void xlsx_endAll(PXML_WRITER writer);

void xlsx_tagOnlyInt(PXML_WRITER writer, const char *tag, int val);
void xlsx_tagOnlyDouble(PXML_WRITER writer, const char *tag, double val);
void xlsx_tagOnlyFloat(PXML_WRITER writer, const char *tag, float val);
void xlsx_tagOnlyString(PXML_WRITER writer, const char *tag, const char *val);

void xlsx_AttrInt(PXML_WRITER writer, const char *attr, int val);
void xlsx_AttrDouble(PXML_WRITER writer, const char *attr, double val);
void xlsx_AttrFloat(PXML_WRITER writer, const char *attr, float val);
void xlsx_AttrString(PXML_WRITER writer, const char *attr, const char *val);

void xlsx_ContentInt(PXML_WRITER writer, int val);
void xlsx_ContentDouble(PXML_WRITER writer, double val);
void xlsx_ContentFloat(PXML_WRITER writer, float val);
void xlsx_ContentString(PXML_WRITER writer, const char *val);


PSTACK 	stackCreate(unsigned int capacity);
int  	stackIsFull(PSTACK stack);
int  	stackIsEmpty(PSTACK stack);
char 	*stackPop(PSTACK stack);
void 	stackPush(PSTACK stack, const char *item);
void	stackFree(PSTACK stack);
void	stackGrow(PSTACK stack);


#endif
