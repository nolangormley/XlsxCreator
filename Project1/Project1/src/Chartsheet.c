#include "../include/Chartsheet.h"
#include "../include/xmlWriter.h"
#include "../include/xlsxHeaders.h"

PCHARTSHEET crtsht_init(PCHARTSHEET chartsht, size_t index, PCHART chart, DRAWING * drawing, PPATH_MANAGER pathmanager)
{
	chartsht = (PCHARTSHEET)calloc(1, sizeof(CHART));
	chartsht->index = index;
	chartsht->chart = chart;
	chartsht->drawing = drawing;
	chartsht->pathManager  = pathmanager;

	return chartsht;
}

int crtsht_save(PCHARTSHEET sheet)
{
	{
		// [- /xl/chartsheets/_rels/sheetX.xml.rels

		char buf[2048];
		snprintf(buf, 2048, "xl/chartsheets/_rels/sheet%d%s", sheet->index, ".xml.rels");
		char * retpath = registerFile(sheet->pathManager, buf);
		PXML_WRITER xml = NULL;
		xlsx_initWriter(&xml, retpath);

		char Target[32];
		snprintf(Target, 32, "../drawings/drawing%d.xml", sheet->drawing->index);

		xlsx_tag(xml, "Relationships");
		xlsx_AttrString(xml, "xmlns", ns_relationships);

		xlsx_tagL(xml, "Relationship");
		xlsx_AttrString(xml, "Id", "rId1");
		xlsx_AttrString(xml, "Type", type_drawing);
		xlsx_AttrString(xml, "Target", Target);
		xlsx_endL(xml);

		xlsx_end(xml, "Relationships");
		// /xl/chartsheets/_rels/sheetX.xml.rels -]
		xlsx_freeWriter(&xml);
	}

	{
		// [- /xl/chartsheets/sheetX.xml
		char buf[64];
		snprintf(buf, 64, "xl/chartsheets/sheet%d%s", sheet->index, ".xml");
		char * retpath = registerFile(sheet->pathManager, buf);
		PXML_WRITER xml = NULL;
		xlsx_initWriter(&xml, retpath);

		xlsx_tag(xml, "chartsheet");
		xlsx_AttrString(xml, "xmlns", ns_book);
		xlsx_AttrString(xml, "xmlns:r", ns_book_r);
		xlsx_tagL(xml, "sheetPr");
		xlsx_endL(xml);

		xlsx_tag(xml, "sheetViews");
		xlsx_tagL(xml, "sheetView");
		xlsx_AttrString(xml, "zoomScale", "85");
		xlsx_AttrString(xml, "workbookViewId", "0");
		xlsx_AttrString(xml, "zoomToFit", "1");
		xlsx_endL(xml);
		xlsx_end(xml, "sheetViews");

		xlsx_tagL(xml, "pageMargins");
		xlsx_AttrString(xml, "left", "0.7");
		xlsx_AttrString(xml, "right", "0.7");
		xlsx_AttrString(xml, "top", "0.75");

		xlsx_AttrString(xml, "bottom", "0.75");
		xlsx_AttrString(xml, "header", "0.3");
		xlsx_AttrString(xml, "footer", "0.3");
		xlsx_endL(xml);

		xlsx_tagL(xml, "drawing");
		xlsx_AttrString(xml, "r:id", "rId1");
		xlsx_endL(xml);

		xlsx_end(xml, "chartsheet");
		// /xl/chartsheets/sheetX.xml -]
		xlsx_freeWriter(&xml);
	}
	return 1;
}
