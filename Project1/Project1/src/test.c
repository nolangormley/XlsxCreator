#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <FPXLSXCreator.h>


int main(int argc, char **argv) {

	PXLSXMAIN main = NULL;
	
	main = initXlsx("./tmp", "filepro.xlsx", "FilePro Test");

	printf("Workbook title: %s\n", main->book->UserName);

	PVECTOR data = vec_init();

	addCellData(data, 13, CELLTYPE_INT, 0);
	addCellData(data, 34, CELLTYPE_INT, 0);
	addCellData(data, 63, CELLTYPE_INT, 0);
	addCellData(data, 19, CELLTYPE_INT, 0);
	addCellData(data, 23, CELLTYPE_INT, 0);
	addCellData(data, 93, CELLTYPE_INT, 0);
	addCellData(data, 33, CELLTYPE_INT, 0);
	addCellData(data, 63, CELLTYPE_INT, 0);
	addCellData(data, 36, CELLTYPE_INT, 0);
	addCellData(data, 56, CELLTYPE_INT, 0);

	addRowData(main->mainsheet, data);

	vec_free(data);
	data = vec_init();

	addCellData(data, 13, CELLTYPE_INT, 0);
	addCellData(data, 34, CELLTYPE_INT, 0);
	addCellData(data, 23, CELLTYPE_INT, 0);
	addCellData(data, 16, CELLTYPE_INT, 0);
	addCellData(data, 21, CELLTYPE_INT, 0);
	addCellData(data, 43, CELLTYPE_INT, 0);
	addCellData(data, 33, CELLTYPE_INT, 0);
	addCellData(data, 62, CELLTYPE_INT, 0);
	addCellData(data, 56, CELLTYPE_INT, 0);
	addCellData(data, 16, CELLTYPE_INT, 0);

	addRowData(main->mainsheet, data);

	createBarChart(main, "FilePro Bar Chart", crt_cell_coordInit(1, 1), crt_cell_coordInit(1, 10));
	createLinearChart(main, "FilePro Linear Chart", crt_cell_coordInit(1, 1), crt_cell_coordInit(1, 10));
	createScatterChart(main, "FilePro Scatter Chart", crt_cell_coordInit(1, 1), crt_cell_coordInit(1, 10), crt_cell_coordInit(2, 1), crt_cell_coordInit(2, 10));

	save(main);

	return 0;
}
