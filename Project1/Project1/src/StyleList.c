#include <StyleList.h>
#include <assert.h>


PSTYLE_LIST sl_init(PSTYLE_LIST stylelist) {
	if (stylelist != NULL) {
		sl_free(stylelist);
	}

	stylelist = (PSTYLE_LIST)calloc(1, sizeof(STYLE_LIST));
	stylelist->fmtLastId = BUILT_IN_STYLES_NUMBER;
	stylelist->borders = vec_init();
	stylelist->fonts = vec_init();
	stylelist->fills = vec_init();
	stylelist->nums = vec_init();
	stylelist->styleIndexes = vec_init();
	stylelist->stylePos = vec_init();

	return stylelist;
}

void sl_free(PSTYLE_LIST stylelist) {
	if (stylelist == NULL) {
		return;
	}

	while (!vec_empty(stylelist->borders)) {
		void *ptr = vec_remove(stylelist->borders, 0);
		free(ptr);
		ptr = NULL;
	}

	while (!vec_empty(stylelist->fonts)) {
		void *ptr = vec_remove(stylelist->fonts, 0);
		free(ptr);
		ptr = NULL;
	}

	while (!vec_empty(stylelist->fills)) {
		void *ptr = vec_remove(stylelist->fills, 0);
		free(ptr);
		ptr = NULL;
	}

	while (!vec_empty(stylelist->nums)) {
		void *ptr = vec_remove(stylelist->nums, 0);
		free(ptr);
		ptr = NULL;
	}

	while (!vec_empty(stylelist->styleIndexes)) {
		void *ptr = vec_remove(stylelist->styleIndexes, 0);
		free(ptr);
		ptr = NULL;
	}

	while (!vec_empty(stylelist->stylePos)) {
		void *ptr = vec_remove(stylelist->stylePos, 0);
		free(ptr);
		ptr = NULL;
	}

	vec_free(stylelist->borders);
	vec_free(stylelist->fonts);
	vec_free(stylelist->fills);
	vec_free(stylelist->nums);
	vec_free(stylelist->styleIndexes);
	vec_free(stylelist->stylePos);

	free(stylelist);
	stylelist = NULL;
}

size_t sl_add(PSTYLE_LIST stylelist, PSTYLE style) {
	size_t styleLinks[STYLE_LINK_NUMBER];
	memset(styleLinks, 0, STYLE_LINK_NUMBER * sizeof(size_t));

	int addItem = 1;

	size_t border_size = stylelist->borders->top + 1;
	for (size_t i = 0; i < border_size; i++) {
		PBORDER border = (PBORDER)stylelist->borders->array[i];

		int match = border_item_eq(border->left, style->border.left);
		match = match && border_item_eq(border->right, style->border.right);
		match = match && border_item_eq(border->bottom, style->border.bottom);
		match = match && border_item_eq(border->top, style->border.top);
		match = match && border_item_eq(border->diagonal, style->border.diagonal);
		match = match && (border->isDiagonalUp == style->border.isDiagonalUp);
		match = match && (border->isDiagonalDown == style->border.isDiagonalDown);

		if (match == 1) {
			addItem = 0;
			styleLinks[STYLE_LINK_BORDER] = i;
			break;
		}
	}

	if (addItem == 1) {
		PBORDER tmp = (PBORDER)calloc(1, sizeof(BORDER));
		memcpy(tmp, &style->border, sizeof(BORDER));
		vec_pushback(stylelist->borders, tmp);
		styleLinks[STYLE_LINK_BORDER] = stylelist->borders->top;
	}

	addItem = 1;
	size_t font_size = stylelist->fonts->top + 1;
	for (size_t i = 0; i < font_size; i++) {
		PFONT font = (PFONT)stylelist->fonts->array[i];

		if (font_eq((*font), style->font)) {
			addItem = 0;
			styleLinks[STYLE_LINK_FONT] = i;
			break;
		}
	}

	if (addItem == 1) {
		PFONT tmp = (PFONT)calloc(1, sizeof(FONT));
		memcpy(tmp, &style->font, sizeof(FONT));
		vec_pushback(stylelist->fonts, tmp);
		styleLinks[STYLE_LINK_FONT] = stylelist->fonts->top;
	}

	addItem = 1;
	size_t fill_size = stylelist->fills->top + 1;
	for (size_t i = 0; i < fill_size; i++) {
		PFILL fill = (PFILL)stylelist->fills->array[i];

		if (fill_eq((*fill), style->fill)) {
			addItem = 0;
			styleLinks[STYLE_LINK_FILL] = i;
			break;
		}
	}

	if (addItem == 1) {
		PFILL tmp = (PFILL)calloc(1, sizeof(FILL));
		memcpy(tmp, &style->fill, sizeof(FILL));
		vec_pushback(stylelist->fonts, tmp);
		styleLinks[STYLE_LINK_FILL] = stylelist->fills->top;
	}

	addItem = 1;
	size_t num_size = stylelist->nums->top + 1;
	for (size_t i = 0; i < num_size; i++) {
		PNUM_FORMAT num = (PNUM_FORMAT)stylelist->nums->array[i];

		if (num_format_eq((*num), style->numFormat)) {
			addItem = 0;
			styleLinks[STYLE_LINK_NUM_FORMAT] = i;
			break;
		}
	}

	if (addItem == 1) {
		if (style->numFormat.id >= BUILT_IN_STYLES_NUMBER) {
			styleLinks[STYLE_LINK_NUM_FORMAT] = stylelist->fmtLastId;
			style->numFormat.id = stylelist->fmtLastId++;
		}
		else {

			styleLinks[STYLE_LINK_NUM_FORMAT] = stylelist->nums->top + 1;
		}

		PNUM_FORMAT tmp = (PNUM_FORMAT)calloc(1, sizeof(NUM_FORMAT));
		memcpy(tmp, &style->numFormat, sizeof(NUM_FORMAT));
		vec_pushback(stylelist->nums, tmp);
	}

	size_t size = stylelist->styleIndexes->top + 1;
	for (int i = 0; (size_t)i < size; i++) {
		size_t link[STYLE_LINK_NUMBER];
		memcpy(link, stylelist->styleIndexes->array[i], STYLE_LINK_NUMBER * sizeof(size_t));

		int match = 1;
		for (int j = 0; j < STYLE_LINK_NUMBER; j++) {
			if (link[j] != styleLinks[j]) {
				match = 0;
				break;
			}
		}

		if (match = 1) {
			return i;
		}
	}

	PTRIPLET pos = (PTRIPLET)calloc(1, sizeof(TRIPLET));
	pos->t1 = style->horizAlign;
	pos->t2 = style->vertAlign;
	pos->t3 = style->wrapText;
	vec_pushback(stylelist->stylePos, pos);

	size_t *link = (size_t *)calloc(STYLE_LINK_NUMBER, sizeof(size_t));
	memcpy(link, styleLinks, STYLE_LINK_NUMBER * sizeof(size_t));
	vec_pushback(stylelist->styleIndexes, link);
	return stylelist->styleIndexes->top;
}
