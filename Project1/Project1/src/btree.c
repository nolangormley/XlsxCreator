#include "../include/btree.h"
#include <string.h>

void insert(PPBTREE tree, uintptr_t id, RECNO recno) {

	PBTREE temp = NULL;

	if (!(*tree)) {
		temp = (PBTREE)calloc(1, sizeof(BTREE));
		temp->_left = temp->_right = NULL;
		temp->_rec = recno;
		temp->_id = id;
		*tree = temp;

		return;
	}

	insert(id < (*tree)->_id ? &(*tree)->_left : &(*tree)->_right, id, recno);
}

void deltree(PBTREE tree) {
	
	if (tree) {
		deltree(tree->_left);
		deltree(tree->_right);
		free(tree);
	}
}

PBTREE getmin(PBTREE node) {
	if (node == NULL) {
		return NULL;
	}

	if (node->_left != NULL) {
		return getmin(node->_left);
	}

	return node;
}

PBTREE delnode(PPBTREE tree, uintptr_t id) {

	if (!(*tree)) {
		// nothing found... return
		return NULL;
	}

	if (id < (*tree)->_id) {
		(*tree)->_left = delnode(&(*tree)->_left, id);
	} else if ( id > (*tree)->_id) {
		(*tree)->_right = delnode(&(*tree)->_right, id);
	} else {
		if ((*tree)->_left == NULL && (*tree)->_right == NULL) {
			free(*tree);
			*tree = NULL;

		} else if((*tree)->_left == NULL) {
			PBTREE temp = (*tree)->_right;
			(*tree) = (*tree)->_right;
			free(temp);

		} else if ((*tree)->_right == NULL) {
			PBTREE temp = (*tree)->_left;
			(*tree) = (*tree)->_left;
			free(temp);

		} else {
			PBTREE temp = getmin((*tree)->_right);
			memcpy((*tree), temp, sizeof(BTREE));
			(*tree)->_right = delnode(&(*tree)->_right, temp->_id);
		}
	}

	return (*tree);
}

PBTREE search(PPBTREE tree, uintptr_t id) {
	
	if (!(*tree)) {
		return NULL;
	}

	if (id < (*tree)->_id) {
		return search(&(*tree)->_left, id);

	} else if (id >(*tree)->_id) {
		return search(&(*tree)->_right, id);

	} else if (id == (*tree)->_id) {
		return *tree;
	}

	// fallback, (should never be hit)
	return NULL;
}

void inorder_action(PPBTREE tree, void (*func)(PBTREE)) {
	if ((*tree)) {
		inorder_action(&(*tree)->_left, func);
		func(*tree);
		inorder_action(&(*tree)->_right, func);
	}
}
