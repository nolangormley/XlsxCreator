#include "../include/vector.h"
#include <assert.h>

PVECTOR vec_init() {
	PVECTOR vec 	= (PVECTOR)calloc(1, sizeof(VECTOR));
	vec->capacity 	= INIT_CAP;
	vec->top 		= -1;
	vec->array 		= (void **)calloc(vec->capacity, sizeof(void **));
	
	return vec;
}

int vec_full(PVECTOR vec) {
	if(vec == NULL) {
		return -1;
	}
	return vec->top == vec->capacity - 1;
}

int vec_empty(PVECTOR vec) {
	if(vec == NULL) {
		return -1;
	}
	
	return vec->top == -1;
}

void *vec_remove(PVECTOR vec, int index) {
	if(vec == NULL) {
		return NULL;
	}
	
	if(vec_empty(vec)) {
		return NULL;
	}
	
	assert(index >= 0 && index < vec->capacity);
	
	void *ptr = vec->array[index];
	
	if(vec->top == index) {
		vec->array[index] == NULL;
		vec->top--;
		return ptr;
	}
	
	memmove(vec->array[index], vec->array[index + 1], (vec->top - index) * sizeof(void *));
	vec->array[vec->top--] = NULL;
	
	return ptr;
}

void vec_pushback(PVECTOR vec, void *data) {
	if(vec == NULL) {
		return;
	}
	
	if(vec_full(vec)) {
		vec_grow(vec);
	}
	
	vec->array[++vec->top] = data;
}

void vec_pushfront(PVECTOR vec, void *data) {
	if(vec == NULL) {
		return;
	}
	
	if(vec_full(vec)) {
		vec_grow(vec);
	}
	
	memmove(vec->array[1], vec->array[0], (vec->top + 1) * sizeof(void *));
	vec->array[0] = data;
	vec->top++;
}

void vec_free(PVECTOR vec) {
	if(vec == NULL) {
		return;
	}
	
	if(vec->array != NULL) {
		free(vec->array);
		vec->array = NULL;
	}
	free(vec);
	vec = NULL;
}

void vec_grow(PVECTOR vec) {
	if(vec == NULL) {
		return;
	}
	
	vec->capacity = vec->capacity * 2;
	vec->array 	= (void **)realloc(vec->array, vec->capacity * sizeof(void **));
	assert(vec->array != NULL);
}

