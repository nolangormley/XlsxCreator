#include <Drawing.h>
#include <xlsxHeaders.h>
#include <xmlWriter.h>

DRAWING * dr_init(DRAWING * draw, size_t index, PPATH_MANAGER pm) {
	assert(draw == NULL);
	assert(pm != NULL);

	draw = (DRAWING *)calloc(1, sizeof(DRAWING));
	draw->index = index;
	draw->pathManager = pm;
	draw->drawings = vec_init();

	return draw;
}

void dr_appendChart(DRAWING * drawing, PCHART chart, DRAWINGPOINT bottomright, DRAWINGPOINT topleft) {
	PDRAWINGINFO CInfo = NULL; // { Chart, NULL, DrawingInfo::absoluteAnchor, DrawingPoint(), DrawingPoint() };
	CInfo = calloc(1, sizeof(DRAWINGINFO));
	CInfo->Chart = chart;
	CInfo->AType = absoluteAnchor;
	if (bottomright.col == NULL) {
		DRAWINGPOINT drpnt;
		drpnt.col = 0;
		drpnt.colOff = 0;
		drpnt.row = 0;
		drpnt.rowOff = 0;

		CInfo->BottomRight = drpnt;
		CInfo->TopLeft = drpnt;
	}

	CInfo->BottomRight = bottomright;
	CInfo->TopLeft = topleft;

	vec_pushback(drawing->drawings, CInfo);
}

int dr_save(DRAWING * draw) {
	return (dr_saveDrawing(draw) && dr_saveDrawingRels(draw));
}

int dr_saveDrawingRels(DRAWING * draw) {
	char FileName[64];
	snprintf(FileName, 64, "xl/drawings/_rels/drawing%d.xml.rels", draw->index);

	char *retpath = registerFile(draw->pathManager, FileName);
	PXML_WRITER xml = NULL;
	xlsx_initWriter(&xml, retpath);
	free(retpath);

	xlsx_tag(xml, "Relationships");
	xlsx_AttrString(xml, "xmlns", ns_relationships);
	int rId = 1;
	for (int i = 0; i <= draw->drawings->top; i++, rId++)
	{
		char Target[32];
		char rIdStream[16];
		const char * TypeString = NULL;
		switch (((PDRAWINGINFO)draw->drawings->array[i])->AType)
		{
		case absoluteAnchor:
		case twoCellAnchor:
		{
			TypeString = type_chart;
			snprintf(Target, 32, "../charts/chart%d.xml", ((PDRAWINGINFO)draw->drawings->array[i])->Chart->index);
			break;
		}
		case imageOneCellAnchor:
		case imageTwoCellAnchor:
		{
			///Images NYI
			break;
		}
		}
		snprintf(rIdStream, 16, "rId%d", rId);

		xlsx_tagL(xml, "Relationship");
		xlsx_AttrString(xml, "Id", rIdStream);
		xlsx_AttrString(xml, "Type", TypeString);
		xlsx_AttrString(xml, "Target", Target);
		xlsx_endL(xml);
	}
	xlsx_end(xml, "Relationships");
	xlsx_freeWriter(&xml);
	return 1;
}

// [- /xl/drawings/drawingX.xml
int dr_saveDrawing(DRAWING * draw) {
	char FileName[64];
	snprintf(FileName, 64, "xl/drawings/drawing%d.xml", draw->index);
	char *retpath = registerFile(draw->pathManager, FileName);
	PXML_WRITER xml = NULL;
	xlsx_initWriter(&xml, retpath);
	free(retpath);

	xlsx_tag(xml, "xdr:wsDr");
	xlsx_AttrString(xml, "xmlns:xdr", ns_xdr);
	xlsx_AttrString(xml, "xmlns:a", ns_a);

	int rId = 1;
	for (int i = 0; i <= draw->drawings->top; i++, rId++)
	{
		switch (((PDRAWINGINFO)draw->drawings->array[i])->AType)
		{
		case absoluteAnchor:
		{
			xlsx_tag(xml, "xdr:absoluteAnchor");

			xlsx_tagL(xml, "xdr:pos");
			xlsx_AttrString(xml, "x", "0");
			xlsx_AttrString(xml, "y", "0");
			xlsx_endL(xml);

			xlsx_tagL(xml, "xdr:ext");
			xlsx_AttrString(xml, "cx", "9312088");
			xlsx_AttrString(xml, "cy", "6084794");
			xlsx_endL(xml);

			dr_saveChartSection(xml, ((PDRAWINGINFO)draw->drawings->array[i])->Chart, rId);
			xlsx_end(xml, "xdr:absoluteAnchor");
			break;
		}
		case twoCellAnchor:
		{
			xlsx_tag(xml, "xdr:twoCellAnchor");

			dr_saveChartPoint(xml, "xdr:from", ((PDRAWINGINFO)draw->drawings->array[i])->TopLeft);
			dr_saveChartPoint(xml, "xdr:to", ((PDRAWINGINFO)draw->drawings->array[i])->BottomRight);
			dr_saveChartSection(xml, ((PDRAWINGINFO)draw->drawings->array[i])->Chart, rId);
			xlsx_end(xml, "xdr:twoCellAnchor");
			break;
		}
		case imageOneCellAnchor:
		{
			xlsx_tag(xml, "xdr:oneCellAnchor");
			xlsx_AttrString(xml, "editAs", "oneCell");

			dr_saveChartPoint(xml, "xdr:from", ((PDRAWINGINFO)draw->drawings->array[i])->TopLeft);
			xlsx_tagL(xml, "xdr:ext");
			xlsx_AttrInt(xml, "cx", ((PDRAWINGINFO)draw->drawings->array[i])->BottomRight.col);
			xlsx_AttrInt(xml, "cy", ((PDRAWINGINFO)draw->drawings->array[i])->BottomRight.row);
			xlsx_endL(xml);

			//SaveImageSection(xml, ((PDRAWINGINFO)draw->drawings->array[i]), rId);
			xlsx_end(xml, "xdr:oneCellAnchor");
			break;
		}
		case imageTwoCellAnchor:
		{
			xlsx_tag(xml, "xdr:twoCellAnchor");
			dr_saveChartPoint(xml, "xdr:from", ((PDRAWINGINFO)draw->drawings->array[i])->TopLeft);
			dr_saveChartPoint(xml, "xdr:to", ((PDRAWINGINFO)draw->drawings->array[i])->BottomRight);
			//SaveImageSection(xml, ((PDRAWINGINFO)draw->drawings->array[i])->Image, rId);
			xlsx_end(xml, "xdr:twoCellAnchor");
			break;
		}
		}
	}
	xlsx_end(xml, "xdr:wsDr");
	xlsx_freeWriter(&xml);
	return 1;
}

void dr_saveChartSection(PXML_WRITER xml, PCHART chart, int rId) {
	char rIdStream[16];
	snprintf(rIdStream, 16, "rId%d", rId);

	xlsx_tag(xml, "xdr:graphicFrame");
	xlsx_AttrString(xml, "macro", "");
	xlsx_tag(xml, "xdr:nvGraphicFramePr");

	xlsx_tagL(xml, "xdr:cNvPr");
	xlsx_AttrInt(xml, "id", rId);
	xlsx_AttrString(xml, "name", chart->title);
	xlsx_endL(xml);

	xlsx_tag(xml, "xdr:cNvGraphicFramePr");
	xlsx_tagL(xml, "a:graphicFrameLocks");
	xlsx_AttrString(xml, "noGrp", "1");
	xlsx_endL(xml);
	xlsx_end(xml, "xdr:cNvGraphicFramePr");

	xlsx_end(xml, "xdr:nvGraphicFramePr");

	xlsx_tag(xml, "xdr:xfrm");

	xlsx_tagL(xml, "a:off");
	xlsx_AttrString(xml, "x", "0");
	xlsx_AttrString(xml, "y", "0");
	xlsx_endL(xml);

	xlsx_tagL(xml, "a:ext");
	xlsx_AttrString(xml, "cx", "0");
	xlsx_AttrString(xml, "cy", "0");
	xlsx_endL(xml);

	xlsx_end(xml, "xdr:xfrm");

	xlsx_tag(xml, "a:graphic");
	xlsx_tag(xml, "a:graphicData");
	xlsx_AttrString(xml, "uri", ns_c);

	xlsx_tagL(xml, "c:chart");
	xlsx_AttrString(xml, "xmlns:c", ns_c);
	xlsx_AttrString(xml, "xmlns:r", ns_book_r);
	xlsx_AttrString(xml, "r:id", rIdStream);
	xlsx_endL(xml);

	xlsx_end(xml, "a:graphicData");
	xlsx_end(xml, "a:graphic");

	xlsx_end(xml, "xdr:graphicFrame");

	xlsx_tagL(xml, "xdr:clientData");
	xlsx_endL(xml);
}

void dr_saveChartPoint(PXML_WRITER xml, const char * Tag, const DRAWINGPOINT Point) {
	xlsx_tag(xml, Tag);

	xlsx_tagOnlyInt(xml, "xdr:col", Point.col);
	xlsx_tagOnlyInt(xml, "xdr:colOff", Point.colOff);
	xlsx_tagOnlyInt(xml, "xdr:row", Point.row);
	xlsx_tagOnlyInt(xml, "xdr:rowOff", Point.rowOff);
	xlsx_end(xml, Tag);
}
