#if defined __unix__ || __unix
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <xmlWriter.h>

void xlsx_initWriter(PXML_WRITER * writer, const char *fileName) {
		if(*writer != NULL) {
			xlsx_freeWriter(*writer);
			*writer= NULL;
		}
		
		assert(fileName != NULL && strlen(fileName));
		
		*writer = (PXML_WRITER)calloc(1, sizeof(XML_WRITER));
		
		(*writer)->fstream = fopen(fileName, "w+");
		if((*writer)->fstream == NULL) {
			xlsx_freeWriter(*writer);
			return;
		}
		
		(*writer)->tags = stackCreate(STACK_DEFAULT);
		if((*writer)->tags == NULL) {
			xlsx_freeWriter(*writer);
			return;
		}
		
		(*writer)->tag_open = 0;
		(*writer)->self_closed = 1;
		
		printf("writer: %p\nwriter->fstream: %p\n", *writer, (*writer)->fstream);
		
		//getchar();
		
		fprintf((*writer)->fstream, "%s\r\n", (const char *)"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
		
		printf("After write\n\n");
}

void xlsx_freeWriter(PXML_WRITER * writer) {
	if(*writer == NULL) {
		return;
	}
	
	xlsx_endAll(*writer);
	
	if((*writer)->fstream != NULL) {
		fclose((*writer)->fstream);
		(*writer)->fstream = NULL;
	}
	
	if ((*writer)->tags != NULL) {
		free((*writer)->tags);
		(*writer)->tags = NULL;
	}
	
	free(*writer);
	*writer = NULL;
}

void xlsx_closeOpenTag(PXML_WRITER writer) {
	assert(writer != NULL);
	if(writer->tag_open == 0) {
		return;
	}
	
	fprintf(writer->fstream, ">");
	writer->tag_open = 0;
}

void xlsx_writeStringEscaped(PXML_WRITER writer, const char *str) {
	assert(writer != NULL);
	for( ; *str; str++) {
		switch(*str) {
		case '&':	fprintf(writer->fstream, "&amp;"); 		break;
		case '<':	fprintf(writer->fstream, "&lt;"); 		break;
		case '>':	fprintf(writer->fstream, "&gt;");		break;
		case '\'':	fprintf(writer->fstream, "&apos;"); 	break;
		case '"':	fprintf(writer->fstream, "&quot;"); 	break;
		default:	fprintf(writer->fstream, "%c", *str); 	break;
		}
	}
}

void xlsx_tag(PXML_WRITER writer, const char *tag) {
	assert(writer != NULL);
	assert(tag != NULL);
	xlsx_closeOpenTag(writer);
	fprintf(writer->fstream, "<%s", tag);
	writer->tag_open 	= 1;
	writer->self_closed = 1;
	stackPush(writer->tags, tag);
}

void xlsx_tagL(PXML_WRITER writer, const char *tag) {
	assert(writer != NULL);
	assert(tag != NULL);
	xlsx_closeOpenTag(writer);
	fprintf(writer->fstream, "<%s", tag);
	writer->tag_open 	= 1;
	writer->self_closed = 0;
}

void xlsx_endL(PXML_WRITER writer) {
	assert(writer != NULL);
	
	fprintf(writer->fstream, "/>");
	writer->tag_open = 0;
}

void xlsx_end(PXML_WRITER writer, const char *tag) {
	assert(writer != NULL);
	assert(!stackIsEmpty(writer->tags));
	
	if(writer->self_closed == 1) {
		fprintf(writer->fstream, "/>");
	} else {
		fprintf(writer->fstream, "</%s>", writer->tags->array[writer->tags->top]);
	}
	
	if(tag != NULL) {
		if(strcmp(writer->tags->array[writer->tags->top], tag) != 0) {
			fprintf(stderr, "Incorrect tag for end: '%s'. Expected '%s'\n", 
				tag,
				writer->tags->array[writer->tags->top]);
				
			assert(strcmp(writer->tags->array[writer->tags->top], tag) == 0);
		}
	}
	
	stackPop(writer->tags);
	writer->tag_open 	= 0;
	writer->self_closed = 0;
}

void xlsx_endAll(PXML_WRITER writer) {
	assert(writer != NULL);
	while(!stackIsEmpty(writer->tags)) {
		xlsx_end(writer, NULL);
	}
}

void xlsx_tagOnlyInt(PXML_WRITER writer, const char *tag, int val) {
	char buf[2048];
	snprintf(buf, 2048, "%d", (long long)val);
	xlsx_tagOnlyString(writer, tag, buf);
}

void xlsx_tagOnlyDouble(PXML_WRITER writer, const char *tag, double val) {
	char buf[2048];
	snprintf(buf, 2048, "%f", val);
	xlsx_tagOnlyString(writer, tag, buf);
}

void xlsx_tagOnlyFloat(PXML_WRITER writer, const char *tag, float val) {
	char buf[2048];
	snprintf(buf, 2048, "%f", val);
	xlsx_tagOnlyString(writer, tag, buf);
}

void xlsx_tagOnlyString(PXML_WRITER writer, const char *tag, const char *val) {
	assert(writer != NULL);
	assert(tag != NULL);
	assert(val != NULL);
	
	xlsx_closeOpenTag(writer);

	fprintf(writer->fstream, "<%s>", tag);
	xlsx_writeStringEscaped(writer, val);
	fprintf(writer->fstream, "</%s>", tag);
	
	writer->self_closed = 0;
}

void xlsx_AttrInt(PXML_WRITER writer, const char *attr, int val) {
	char buf[2048];
	snprintf(buf, 2048, "%d", (long long)val);
	xlsx_AttrString(writer, attr, buf);
}

void xlsx_AttrDouble(PXML_WRITER writer, const char *attr, double val) {
	char buf[2048];
	snprintf(buf, 2048, "%f", val);
	xlsx_AttrString(writer, attr, buf);
}

void xlsx_AttrFloat(PXML_WRITER writer, const char *attr, float val) {
	char buf[2048];
	snprintf(buf, 2048, "%f", val);
	xlsx_AttrString(writer, attr, buf);
}

void xlsx_AttrString(PXML_WRITER writer, const char *attr, const char *val) {
	assert(writer != NULL);
	assert(attr != NULL);
	assert(val != NULL);
	
	assert(writer->tag_open);
	fprintf(writer->fstream, " %s=\"", attr);
	xlsx_writeStringEscaped(writer, val);
	fprintf(writer->fstream, "\"");	
}

void xlsx_ContentInt(PXML_WRITER writer, int val) {
	char buf[2048];
	snprintf(buf, 2048, "%d", (long long)val);
	xlsx_ContentString(writer, buf);
}

void xlsx_ContentDouble(PXML_WRITER writer, double val) {
	char buf[2048];
	snprintf(buf, 2048, "%f", val);
	xlsx_ContentString(writer, buf);
}

void xlsx_ContentFloat(PXML_WRITER writer, float val) {
	char buf[2048];
	snprintf(buf, 2048, "%f", val);
	xlsx_ContentString(writer, buf);
}

void xlsx_ContentString(PXML_WRITER writer, const char *val) {
	assert(writer != NULL);
	assert(val != NULL);
	
	xlsx_closeOpenTag(writer);
	xlsx_writeStringEscaped(writer, val);
	
	writer->self_closed = 0;
}



// Stack operations
PSTACK stackCreate(unsigned int capacity) {
	PSTACK stack 	= (PSTACK)calloc(1, sizeof(STACK));
	stack->capacity = capacity;
	stack->top 		= -1;
	stack->array 	= (char **)calloc(256, sizeof(char **));
	//stack->array    = (char **)calloc(1, (sizeof(char * ) * stack->capacity));
	
	return stack;
}

int stackIsFull(PSTACK stack) {
	if(stack == NULL) {
		return -1;
	}
	return stack->top == stack->capacity - 1;
}

int stackIsEmpty(PSTACK stack) {
	if(stack == NULL) {
		return -1;
	}
	
	return stack->top == -1;
}

char *stackPop(PSTACK stack) {
	if(stack == NULL) {
		return NULL;
	}
	
	if(stackIsEmpty(stack)) {
		return NULL;
	}
	
	return stack->array[stack->top--];
}

void stackPush(PSTACK stack, const char *item) {
	if(stack == NULL) {
		return;
	}
	
	if(stackIsFull(stack)) {
		stackGrow(stack);
	}
	
	stack->array[++stack->top] = (char *)item;
}

void stackFree(PSTACK stack) {
	if(stack == NULL) {
		return;
	}
	
	if(stack->array != NULL) {
		free(stack->array);
		stack->array = NULL;
	}
	free(stack);
	stack = NULL;
}

void stackGrow(PSTACK stack) {
	if(stack == NULL) {
		printf("Stack is null in stackGrow()\n");
		return;
	}
	
	printf("Stack cap: %d, Stack new cap: %d\n", stack->capacity, stack->capacity * 2);
	
	stack->capacity = stack->capacity * 2;
	stack->array 	= (char **)realloc(stack->array, stack->capacity * sizeof(char **));
	assert(stack->array != NULL);
}
