#include "../include/PathManager.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/syscall.h>
#endif

char formatA[] = "%s%s";
char formatB[] = "%s/%s";

PPATH_MANAGER pm_init(PPATH_MANAGER manager, const char *path) {
	if(manager != NULL) {
		pm_free(manager);
	}
	
	assert(path != NULL && strlen(path));
	
	manager = (PPATH_MANAGER)calloc(1, sizeof(PATH_MANAGER));
	snprintf(manager->tempPath, (size_t)PATH_MAX, "%s\0", path);
	manager->tempDirs		= vec_init();
	manager->contentFiles	= vec_init();
	
	return manager;
}

void pm_free(PPATH_MANAGER manager) {
	if(manager == NULL) {
		return;
	}
	
	pm_clearTemp(manager);
	
	vec_free(manager->tempDirs);
	vec_free(manager->contentFiles);
	free(manager);
	manager = NULL;
}

int makeDirectory(PPATH_MANAGER manager, const char *path) {
	int retval = 0;
	size_t orig_len = strlen(path);
	char *tmp = calloc(orig_len * 2, sizeof(char));
	const char *p = path;
	char *t = tmp;
	
	int skip = 0;
	for( ; *p; p++) {
		if(skip) {
			skip = 0;
			continue;
		}
		
		if(*p == '\\') {
			skip = 1;
			*t = '/';
			t++;
			continue;
		}
		
		*t = *p;
		t++;
	}
	
	size_t len;
	len = strlen(tmp);
	if(tmp[len - 1] == '/')
		tmp[len - 1] = 0;
	for(t = tmp + 1; *t; t++) {
		if(*t == '/') {
			*t = 0;
#ifdef _WIN32
			retval = mkdir(tmp);
#else
			retval = mkdir(tmp, 0777);
#endif
			if(retval == 0 ) {
				char *dir = calloc(strlen(tmp) + 1, sizeof(char));
				memcpy(dir, tmp, strlen(tmp) + 1);
				vec_pushback(manager->tempDirs, dir);
			}
            if((retval == -1) && (errno == ENOENT)) { 
				free(tmp);
				return 0;
			}
			*t = '/';
		}
	}

#ifdef _WIN32
	retval = mkdir(tmp);
#else
	retval = mkdir(tmp, 0777);
#endif
	if(retval == 0) {
		char *dir = calloc(strlen(tmp) + 1, sizeof(char));
		memcpy(dir, tmp, strlen(tmp) + 1);
		vec_pushback(manager->tempDirs, dir);
	}
	if((retval == -1) && (errno == ENOENT)) { 
		free(tmp);
		return 0;
	}

	free(tmp);
	return 1;
}

// BUF NEEDS FREED AT SOME POINT
char *registerFile(PPATH_MANAGER manager, const char *path) {
	size_t len = strlen(manager->tempPath) + strlen(path) + 16;
	char *buf = calloc(len, sizeof(char));
	char *format = 	(manager->tempPath[strlen(manager->tempPath) - 1] == '/' || 
					manager->tempPath[strlen(manager->tempPath) - 1]  == '\\' ) ?
					formatA : formatB;
	
	snprintf(buf, len, format, manager->tempPath, path);
	
	int hld = strlen(buf) - 1;
	char which = 0;
	while(hld >= 0) {
		
		if(buf[hld] == '/') {
			which = '/';
			break;
		} else if(buf[hld] == '\\') {
			which = '\\';
			break;
		}
		
		hld--;
	}

	if(hld > -1) {
		buf[hld] = 0;
	}
	
	makeDirectory(manager, buf);
	
	if(hld > -1) {
		buf[hld] = which;
	}
	
	char *tmp = calloc(strlen(path) + 1, sizeof(char));
	memcpy(tmp, path, strlen(path) + 1);

	vec_pushback(manager->contentFiles, tmp);
	
	return buf;
}

void pm_clearTemp(PPATH_MANAGER manager) {
	char buf[2048];
	
	char *format = 	(manager->tempPath[strlen(manager->tempPath) - 1] == '/' || 
					manager->tempPath[strlen(manager->tempPath) - 1]  == '\\' ) ?
					formatA : formatB;
	
	while(!vec_empty(manager->contentFiles)) {
		char *ptr = (char *)vec_remove(manager->contentFiles, manager->contentFiles->top);
		snprintf(buf, 2048, format, manager->tempPath, ptr);
		remove(buf);
		free(ptr);
	}
	
	while(!vec_empty(manager->tempDirs)) {
		char *ptr = (char *)vec_remove(manager->tempDirs, manager->tempDirs->top);
		rmdir(ptr);
		free(ptr);
	}
}
