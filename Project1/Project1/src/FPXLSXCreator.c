#include <FPXLSXCreator.h>

PXLSXMAIN initXlsx(char * tmppath, char * filename, char * sheetname) {
	PXLSXMAIN main = NULL;
	main = (PXLSXMAIN)calloc(1, sizeof(XLSXMAIN));
	main->pm = pm_init(main->pm, tmppath);
	wrkbk_init(&main->book, main->pm, "FilePro", filename);
	main->mainsheet = wrkbk_addSheet(main->book, sheetname);
	return main;
}

void addCellData(PVECTOR vec, void * value, size_t type, size_t style) {
	PCELL_DATA_VOID data = NULL;
	data = (PCELL_DATA_VOID)calloc(1, sizeof(CELL_DATA_VOID));
	data->type = type;
	data->value = value;
	data->style = style;
	vec_pushback(vec, data);
}

void addRowData(PWORKSHEET sheet, PVECTOR vec) {
	ws_addRow(sheet, vec, sheet->offset_column, 0);
}

int createBarChart(PXLSXMAIN main, char * title, PCELL_COORD valFrom, PCELL_COORD valTo) {
	PCHARTSHEET crtsheet = wrkbk_addChartSheet(main->book, title, CHART_BAR);
	PSERIES ser = NULL;
	ser = crt_seriesInit(ser);
	ser->valSheet = main->mainsheet;
	ser->catSheet = NULL;
	ser->valAxisFrom = valFrom;
	ser->valAxisTo = valTo;
	ser->title = title;

	crtsheet->chart->diagram->barDir = BAR_DIR_HORIZONTAL;
	crtsheet->chart->diagram->barGroup = BAR_GROUP_CLUSTERED;
	crtsheet->chart->diagram->tableData = TBL_DATA;
	crtsheet->chart->yAxis->isVal = 0 ;

	crt_addSeries(crtsheet->chart, ser, 1);
}

int createLinearChart(PXLSXMAIN main, char * title, PCELL_COORD valFrom, PCELL_COORD valTo) {
	PCHARTSHEET crtsheet = wrkbk_addChartSheet(main->book, title, CHART_LINEAR);
	PSERIES ser = NULL;
	ser = crt_seriesInit(ser);
	ser->valSheet = main->mainsheet;
	ser->catSheet = NULL;
	ser->valAxisFrom = valFrom;
	ser->valAxisTo = valTo;
	ser->title = title;

	//ser->JoinType = joinLine;
	//ser->DashType = dashSolid;

	ser->Marker->Type = symDiamond;

	crt_addSeries(crtsheet->chart, ser, 1);
}

int createScatterChart(PXLSXMAIN main, char * title, PCELL_COORD valFrom, PCELL_COORD valTo, PCELL_COORD catFrom, PCELL_COORD catTo) {
	PCHARTSHEET crtsheet = wrkbk_addChartSheet(main->book, title, CHART_SCATTER);
	PSERIES ser = NULL;
	ser = crt_seriesInit(ser);
	ser->valSheet = main->mainsheet;
	ser->catSheet = main->mainsheet;
	ser->catAxisFrom = catFrom;
	ser->catAxisTo = catTo;
	ser->valAxisFrom = valFrom;
	ser->valAxisTo = valTo;
	ser->title = "series test";

	//ser->JoinType = joinLine;
	//ser->DashType = dashSolid;

	ser->Marker->Type = symDiamond;

	crtsheet->chart->diagram->legend_pos = POS_RIGHT;
	crtsheet->chart->xAxis->gridLines = GRID_MAJOR_N_MINOR;
	crtsheet->chart->yAxis->gridLines = GRID_MAJOR_N_MINOR;
	crtsheet->chart->diagram->tableData = TBL_DATA;
	crtsheet->chart->diagram->name = title;

	crt_addSeries(crtsheet->chart, ser, 1);
}

int save(PXLSXMAIN main) {
	wrkbk_saveCore(main->book);
	wrkbk_saveApp(main->book);
	wrkbk_saveContentType(main->book);
	wrkbk_saveTheme(main->book);
	//wrkbk_saveStyles(book);
	wrkbk_saveSharedStrings(main->book);
	wrkbk_saveWorkbook(main->book);
	wrkbk_save(main->book);
}
