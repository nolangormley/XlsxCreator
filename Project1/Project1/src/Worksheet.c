#include <Drawing.h>
#include <Worksheet.h>
#include <xlsxHeaders.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>


PWORKSHEET ws_init(PWORKSHEET worksheet, struct DRAWING * drawing, PPATH_MANAGER manager, size_t index,
					uint32_t frozenWidth, uint32_t frozenHeight, PVECTOR colWidths) {
	if (worksheet != NULL) {
		ws_free(worksheet);
	}

	assert(manager != NULL);

	worksheet = (PWORKSHEET)calloc(1, sizeof(WORKSHEET));
	worksheet->initialized = 1;
	worksheet->row_opened = 0;
	worksheet->cur_column = 0;
	worksheet->offset_column = 0;
	worksheet->index = index;
	worksheet->title = "Sheet1";
	worksheet->withFormula = 0;
	worksheet->withComments = 0;
	worksheet->calcChain = vec_init();
	worksheet->comments = vec_init();
	worksheet->mergedCells = vec_init();
	worksheet->sharedStrings = NULL;
	worksheet->row_index = 0;
	worksheet->orientation = PAGE_PORTRAIT;
	worksheet->pathManager = manager;
	worksheet->drawing = drawing;

	char buf[2048];
	snprintf(buf, 2048, "xl/worksheets/sheet%d%s\0", worksheet->index, ".xml");
	char *retpath = registerFile(worksheet->pathManager, buf);

	xlsx_initWriter(&worksheet->xmlWriter, retpath);
	PXML_WRITER xml = worksheet->xmlWriter;
	free(retpath);

	xlsx_tag(xml, "worksheet");
	xlsx_AttrString(xml, "xmlns", ns_book);
	xlsx_AttrString(xml, "xmlns:r", ns_book_r);
	xlsx_AttrString(xml, "xmlns:mc", ns_mc);
	xlsx_AttrString(xml, "mc:Ignorable", "x14ac");
	xlsx_AttrString(xml, "xmlns:x14ac", ns_x14ac);

	xlsx_tagL(xml, "dimension");
	xlsx_AttrString(xml, "ref", "A1");
	xlsx_endL(xml);

	xlsx_tag(xml, "sheetViews");
	xlsx_tag(xml, "sheetView");
	xlsx_AttrString(xml, "tabSelected", "0");
	xlsx_AttrString(xml, "workbookViewId", "0");

	if (frozenWidth != 0 || frozenHeight != 0) {
		ws_addFrozenPane(worksheet, frozenWidth, frozenHeight);
	}

	xlsx_end(xml, "sheetView");
	xlsx_end(xml, "sheetViews");

	xlsx_tagL(xml, "sheetFormatPr");
	xlsx_AttrString(xml, "defaultRowHeight", "15");
	xlsx_AttrString(xml, "x14ac:dyDescent", "0.25");
	xlsx_endL(xml);



	if (colWidths != NULL && !vec_empty(colWidths)) {
		xlsx_tag(xml, "cols");
		for (int i = 0; i <= colWidths->top; i++) {
			PCOLUMN_WIDTH pcw = (PCOLUMN_WIDTH)colWidths->array[i];

			xlsx_tagL(xml, "col");
			xlsx_AttrInt(xml, "min", pcw->colFrom + 1);
			xlsx_AttrInt(xml, "max", pcw->colTo + 1);
			xlsx_AttrFloat(xml, "width", pcw->width);
			xlsx_endL(xml);
		}
		xlsx_end(xml, "cols");
	}

	xlsx_tag(xml, "sheetData");

	return worksheet;
}

void ws_free(PWORKSHEET worksheet) {
	if (worksheet == NULL) {
		return;
	}

	// tbd
	// .
	// .
	// .
	if (worksheet->xmlWriter != NULL) {
		xlsx_freeWriter(worksheet->xmlWriter);
	}

	// THIS NEEDS TO BE IMPLEMENTED CORRECTLY
	vec_free(worksheet->calcChain);
	vec_free(worksheet->comments);
	vec_free(worksheet->mergedCells);
	vec_free(worksheet->sharedStrings);

	free(worksheet);
	worksheet = NULL;
}

void ws_addRow(PWORKSHEET sheet, PVECTOR data, uint32_t offset, uint32_t height) {
	PXML_WRITER xml = sheet->xmlWriter;
	sheet->offset_column = offset;

	char Spans[32];

	snprintf(Spans, 32, "%d:%d", sheet->offset_column + 1, data->top + 1 + sheet->offset_column);

	xlsx_tag(xml, "row");
	xlsx_AttrInt(xml, "r", ++sheet->row_index);
	xlsx_AttrString(xml, "spans", Spans);
	xlsx_AttrString(xml, "x14ac:dyDescent", "0.25");

	if (height != 0) {
		xlsx_AttrInt(xml, "ht", height);
		xlsx_AttrString(xml, "customHeight", "1");
	}
	sheet->cur_column = 0;

	ws_addCells(sheet, data);

	xlsx_end(xml, "row");

	sheet->offset_column = 0;
}

void ws_beginRow(PWORKSHEET worksheet, uint32_t height) {
	assert(worksheet != NULL);
	if (worksheet->row_opened == 1) {
		xlsx_end(worksheet->xmlWriter, "row");
	}

	PXML_WRITER xml = worksheet->xmlWriter;

	xlsx_tag(xml, "row");
	xlsx_AttrInt(xml, "r", ++worksheet->row_index);
	xlsx_AttrString(xml, "x14ac:dyDescent", "0.25");

	if (height != 0) {
		xlsx_AttrInt(xml, "ht", height);
		xlsx_AttrInt(xml, "customHeight", 1);
	}

	worksheet->cur_column = 0;
	worksheet->row_opened = 1;
}

void ws_endRow(PWORKSHEET worksheet) {
	assert(worksheet != NULL);

	if (!worksheet->row_opened) {
		return;
	}
	xlsx_end(worksheet->xmlWriter, "row");
	worksheet->row_opened = 0;
}

// NEEDS TO BE FREED AT SOME POINT
char *ws_internal_toCellCoords(int32_t row, int32_t col) {
	const int32_t iAlphLen = 26;
	const char *szAlph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	int32_t div = col / iAlphLen;
	size_t size = 2048;
	char *buf = (char *)calloc(size, sizeof(char));

	buf[0] = szAlph[col % iAlphLen];

	size_t idx = 1;
	while (1) {
		if (div == 0) {
			break;
		}

		if (idx > (size - 64)) {
			size *= 2;
			buf = (char *)realloc(buf, size * sizeof(char));
			assert(buf != NULL);
		}

		snprintf(buf, size, "%c%s", szAlph[div % iAlphLen], buf);
		div /= iAlphLen;
		idx++;
	}

	snprintf(buf, size, "%s%d\0", buf, row);
	return buf;
}

void ws_internal_addCell(PWORKSHEET worksheet, const char *value, size_t style) {
	assert(worksheet != NULL);
	assert(value != NULL);

	PXML_WRITER xml = worksheet->xmlWriter;
	char *ccoords = ws_internal_toCellCoords(worksheet->row_index, worksheet->offset_column + worksheet->cur_column);

	xlsx_tag(xml, "c");
	xlsx_AttrString(xml, "r", ccoords);

	if (style != 0) {
		xlsx_AttrInt(xml, "s", style);
	}
	xlsx_tagOnlyString(xml, "v", value);
	xlsx_end(xml, "c");

	worksheet->cur_column++;

	free(ccoords);
}

void ws_addCells(PWORKSHEET worksheet, PVECTOR cells) {
	PCELL_DATA_VOID tmp = NULL;
	for (int i = 0; i <= cells->top; i++) {
		tmp = ((PCELL_DATA_VOID)cells->array[i]);
		switch (tmp->type) {
		case CELLTYPE_INT:
			ws_addCellIntR(worksheet, (int32_t)tmp->value, tmp->style);
			break;
		case CELLTYPE_STR:
			ws_addCellStrR(worksheet, (char *)tmp->value, tmp->style);
		}
	}
}

void ws_addCellInlnStr(PWORKSHEET worksheet, PCELL_DATA_STR data) {
	ws_addCellInlnStrR(worksheet, data->value, data->style);
}

void ws_addCellStr(PWORKSHEET worksheet, PCELL_DATA_STR data) {
	ws_addCellStrR(worksheet, data->value, data->style);
}

void ws_addCellTime(PWORKSHEET worksheet, PCELL_DATA_TIME data) {
	ws_addCellTimeR(worksheet, data->value, data->style);
}

void ws_addCellInt(PWORKSHEET worksheet, PCELL_DATA_INT data) {
	ws_addCellIntR(worksheet, data->value, data->style);
}

void ws_addCellUInt(PWORKSHEET worksheet, PCELL_DATA_UINT data) {
	ws_addCellUIntR(worksheet, data->value, data->style);
}

void ws_addCellDbl(PWORKSHEET worksheet, PCELL_DATA_DBL data) {
	ws_addCellDblR(worksheet, data->value, data->style);
}

void ws_addCellFlt(PWORKSHEET worksheet, PCELL_DATA_FLT data) {
	ws_addCellFltR(worksheet, data->value, data->style);
}

void ws_addCellInlnStrR(PWORKSHEET worksheet, const char *value, size_t style) {
	assert(worksheet != NULL);
	assert(value != NULL);

	PXML_WRITER xml = worksheet->xmlWriter;
	char *ccoords = ws_internal_toCellCoords(worksheet->row_index, worksheet->offset_column + worksheet->cur_column);

	xlsx_tag(xml, "c");
	xlsx_AttrString(xml, "r", ccoords);
	xlsx_AttrString(xml, "t", "inlineStr");
	if (style != 0) {
		xlsx_AttrInt(xml, "s", style);
	}
	xlsx_tag(xml, "is");
	xlsx_tagOnlyString(xml, "t", value);
	xlsx_end(xml, "is");
	xlsx_end(xml, "c");

	worksheet->cur_column++;

	free(ccoords);
}

void ws_addCellStrR(PWORKSHEET worksheet, const char *value, size_t style) {
	assert(worksheet != NULL);
	assert(value != NULL);

	PXML_WRITER xml = worksheet->xmlWriter;
	if (strlen(value) != 0) {

		char *ccoords = ws_internal_toCellCoords(worksheet->row_index, worksheet->offset_column + worksheet->cur_column);

		xlsx_tag(xml, "c");
		xlsx_AttrString(xml, "r", ccoords);
		if (style != 0) {
			xlsx_AttrInt(xml, "s", style);
		}

		int remove_now = 1;
		if (value[0] == '=') {
			xlsx_tagOnlyString(xml, "f", value + 1);
			worksheet->withFormula = 1;
			vec_pushback(worksheet->calcChain, ccoords);
			remove_now = 0;
		}
		else {

			if (worksheet->sharedStrings != NULL) {

				int found = 0;

				uint64_t str_index = -1;
				for (int i = 0; i <= worksheet->sharedStrings->top; i++) {
					if (strcmp((char *)worksheet->sharedStrings->array[i], value) == 0) {
						str_index = i;
						found = 1;
						break;
					}
				}

				if (!found) {
					char *tmp = calloc(strlen(value) + 1, sizeof(char));
					memcpy(tmp, value, strlen(value) + 1);
					vec_pushback(worksheet->sharedStrings, (void *)value);
					str_index = worksheet->sharedStrings->top;
				}

				xlsx_AttrString(xml, "t", "s");
				xlsx_tagOnlyInt(xml, "v", str_index);

			}
			else {
				xlsx_tagOnlyString(xml, "v", value);
			}
		}
		xlsx_end(xml, "c");

		if (remove_now == 1) {
			free(ccoords);
		}

	}
	else if (style != 0) {
		char *ccoords = ws_internal_toCellCoords(worksheet->row_index, worksheet->offset_column + worksheet->cur_column);
		xlsx_tag(xml, "c");
		xlsx_AttrString(xml, "r", ccoords);
		xlsx_AttrInt(xml, "s", style);
		xlsx_end(xml, "c");
	}

	worksheet->cur_column++;
}

void ws_addCellTimeR(PWORKSHEET worksheet, time_t value, size_t style) {
	const int64_t secondsFrom1900to1970 = 2208988800u;
	const double excelOneSecond = 0.0000115740740740741;

	struct tm * t = localtime(&value);

	time_t timeSinceEpoch = t->tm_sec + t->tm_min * 60 + t->tm_hour * 3600 + t->tm_yday * 86400 +
		(t->tm_year - 70) * 31536000 + ((t->tm_year - 69) / 4) * 86400 -
		((t->tm_year - 1) / 100) * 86400 + ((t->tm_year + 299) / 400) * 86400;

	double CalcedValue = excelOneSecond * (secondsFrom1900to1970 + timeSinceEpoch) + 2;

	char buf[2048];
	snprintf(buf, 2048, "%0.10f\0", CalcedValue);
	ws_internal_addCell(worksheet, buf, style);
}

void ws_addCellIntR(PWORKSHEET worksheet, int32_t value, size_t style) {
	char buf[2048];
	snprintf(buf, 2048, "%d\0", value);
	ws_internal_addCell(worksheet, buf, style);
}

void ws_addCellUIntR(PWORKSHEET worksheet, uint32_t value, size_t style) {
	char buf[2048];
	snprintf(buf, 2048, "%u\0", value);
	ws_internal_addCell(worksheet, buf, style);
}

void ws_addCellDblR(PWORKSHEET worksheet, double value, size_t style) {
	char buf[2048];
	snprintf(buf, 2048, "%f\0", value);
	ws_internal_addCell(worksheet, buf, style);
}

void ws_addCellFltR(PWORKSHEET worksheet, float value, size_t style) {
	char buf[2048];
	snprintf(buf, 2048, "%f\0", value);
	ws_internal_addCell(worksheet, buf, style);
}

void ws_addRowStr(PWORKSHEET worksheet, PCELL_DATA_STR *data, size_t count, uint32_t offset, uint32_t height) {} 	// NYI
void ws_addRowTime(PWORKSHEET worksheet, PCELL_DATA_TIME *data, size_t count, uint32_t offset, uint32_t height) {} 	// NYI
void ws_addRowInt(PWORKSHEET worksheet, PCELL_DATA_INT *data, size_t count, uint32_t offset, uint32_t height) {} 	// NYI
void ws_addRowUInt(PWORKSHEET worksheet, PCELL_DATA_UINT *data, size_t count, uint32_t offset, uint32_t height) {} 	// NYI
void ws_addRowDbl(PWORKSHEET worksheet, PCELL_DATA_DBL *data, size_t count, uint32_t offset, uint32_t height) {} 	// NYI
void ws_addRowFlt(PWORKSHEET worksheet, PCELL_DATA_FLT *data, size_t count, uint32_t offset, uint32_t height) {}	 	// NYI

void ws_mergeCells(PWORKSHEET worksheet, CELL_COORD from, CELL_COORD to) {
	assert(worksheet != NULL);
	if (from.row == 0 || to.row == 0) {
		return;
	}

	char *c1 = ws_internal_toCellCoords(from.row, from.col);
	char *c2 = ws_internal_toCellCoords(to.row, to.col);

	char *buf = (char *)calloc(strlen(c1) + strlen(c2) + 2, sizeof(char));
	sprintf(buf, "%s:%s\0", c1, c2);

	free(c1);
	free(c2);

	vec_pushback(worksheet->mergedCells, buf);
}

void ws_addFrozenPane(PWORKSHEET worksheet, uint32_t frozenWidth, uint32_t frozenHeight) {}	 	// NYI

																								// This will also handle some of the cleanup we would have to handle otherwise.
int ws_save(PWORKSHEET worksheet) {
	assert(worksheet != NULL);

	PXML_WRITER xml = worksheet->xmlWriter;
	xlsx_end(xml, "sheetData");

	PVECTOR merge = worksheet->mergedCells;

	if (!vec_empty(merge)) {
		xlsx_tag(xml, "mergeCells");
		xlsx_AttrInt(xml, "count", merge->top + 1);
		while (!vec_empty(merge)) {
			char *ptr = (char *)vec_remove(merge, 0);
			xlsx_tagL(xml, "mergeCell");
			xlsx_AttrString(xml, "ref", ptr);
			xlsx_endL(xml);

			free(ptr);
			ptr = NULL;
		}
		xlsx_end(xml, "mergeCells");
	}

	xlsx_tagL(xml, "pageMargins");
	xlsx_AttrString(xml, "left", "0.7");
	xlsx_AttrString(xml, "right", "0.7");
	xlsx_AttrString(xml, "top", "0.75");
	xlsx_AttrString(xml, "bottom", "0.75");
	xlsx_AttrString(xml, "header", "0.3");
	xlsx_AttrString(xml, "footer", "0.3");
	xlsx_endL(xml);

	xlsx_tagL(xml, "pageSetup");
	xlsx_AttrString(xml, "paperSize", "9"); // A4 paper

	if (worksheet->orientation == PAGE_PORTRAIT) {
		xlsx_AttrString(xml, "orientation", "portrait");
	}
	else if (worksheet->orientation == PAGE_LANDSCAPE) {
		xlsx_AttrString(xml, "orientation", "landscape");
	}

	xlsx_endL(xml);

	size_t rId = 1;
	// TBD Drawing

	if (worksheet->withComments == 1) {
		char rIdBuf[2048];
		snprintf(rIdBuf, 2048, "rId%d\0", rId);

		xlsx_tagL(xml, "legacyDrawing");
		xlsx_AttrString(xml, "r:id", rIdBuf);
		xlsx_endL(xml);

		rId += 2;
	}

	xlsx_end(xml, "worksheet");

	xlsx_freeWriter(&worksheet->xmlWriter);
	xml = worksheet->xmlWriter = NULL;

	if (rId != 1 && !ws_saveSheetRels(worksheet)) {
		return 0;
	}

	worksheet->initialized = 0;
	return 1;
}

int ws_saveSheetRels(PWORKSHEET worksheet) {

	char buf[2048];
	snprintf(buf, 2048, "xl/worksheets/_rels/sheet%d%s\0", worksheet->index, ".xml.rels");
	char *retpath = registerFile(worksheet->pathManager, buf);

	PXML_WRITER xml = worksheet->xmlWriter;
	xlsx_initWriter(&xml, retpath);
	free(retpath);

	xlsx_tag(xml, "Relationships");
	xlsx_AttrString(xml, "xmlns", ns_relationships);

	size_t rId = 1;
	// TBD Drawing

	if (worksheet->withComments == 1) {
		char vml[2048];
		char com[2048];
		char ridv[2048];
		char ridc[2048];

		snprintf(vml, 2048, "../drawings/vmlDrawing%d%s\0", worksheet->index, ".vml");
		snprintf(com, 2048, "../comments%d%s\0", worksheet->index, ".xml");
		snprintf(ridv, 2048, "rId%d\0", rId);
		snprintf(ridc, 2048, "rId%d\0", rId + 1);

		rId += 2;

		xlsx_tagL(xml, "Relationship");
		xlsx_AttrString(xml, "Id", ridv);
		xlsx_AttrString(xml, "Type", type_vml);
		xlsx_AttrString(xml, "Target", vml);
		xlsx_endL(xml);

		xlsx_tagL(xml, "Relationship");
		xlsx_AttrString(xml, "Id", ridc);
		xlsx_AttrString(xml, "Type", type_comments);
		xlsx_AttrString(xml, "Target", com);
		xlsx_endL(xml);
	}

	xlsx_end(xml, "Relationships");

	xlsx_freeWriter(&xml);

	return 1;
}
