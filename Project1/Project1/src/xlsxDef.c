#include <xlsxDef.h>
#include <stdio.h>
#include <vector.h>

void cell_coord_toString(char retValBuf[], PCELL_COORD cell) {
	int iAlphLen = 26;
	char* szAlph[26] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
		"M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
	char tmp[4];
	int div = cell->col;
	int i;
	
	for (i = 0; div > iAlphLen; i++) {
		div %= iAlphLen;
	}

	if (i != 0) {
		snprintf(retValBuf, 4, "%s%s%d", szAlph[0], szAlph[div], cell->row);
	}
	snprintf(retValBuf, 4, "%s%d", szAlph[div-1], cell->row);
}
