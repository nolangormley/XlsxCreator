#include <Workbook.h>
#include <common.h>
#include <xlsxHeaders.h>
#include <xmlWriter.h>
#include <vector.h>
#include <Chartsheet.h>
#include <Drawing.h>
#include <zip.h>
#include <assert.h>

void wrkbk_init(PWORKBOOK *book, PPATH_MANAGER pm, char* userName, char * filename) {
	PWORKBOOK tmp = (PWORKBOOK)calloc(1, sizeof(WORKBOOK));


	tmp->pathManager = pm;
	tmp->UserName = userName;

	tmp->commLastId = 0;
	tmp->sheetId = 1;
	tmp->activeSheetIndex = 0;

	tmp->temp_path = "./tmp/";

	tmp->worksheets = vec_init();
	tmp->chartsheets = vec_init();
	tmp->charts = vec_init();
	tmp->sharedStrings = vec_init();
	tmp->drawings = vec_init();
	//tmp->styleList = sl_init(tmp->styleList);

	tmp->zip = zip_open(filename, ZIP_DEFAULT_COMPRESSION_LEVEL, 'w');

	assert(tmp->zip != NULL);

	book[0] = tmp;
}

int zip_addFile(struct zip_t * zip, char * path, char * name) {
	zip_entry_open(zip, name);
	{
		zip_entry_fwrite(zip, path);
	}
	return zip_entry_close(zip);
}

int wrkbk_save(PWORKBOOK book) {

	//if (!SaveCore() || !SaveApp() || !SaveContentType() || !SaveTheme() ||
	//	!SaveComments() || !SaveSharedStrings() || !SaveStyles() || !SaveWorkbook())
	//	return -10;

	for (int i = 0; i <= book->worksheets->top; i++) {
		if (ws_save((PWORKSHEET)(book->worksheets->array[i])) != 1) return -1;
		ws_saveSheetRels((PWORKSHEET)(book->worksheets->array[i]));
	}

	for (int i = 0; i <= book->chartsheets->top; i++) {
		if (crtsht_save((PCHARTSHEET)(book->chartsheets->array[i])) != 1) return -1;
	}

	for (int i = 0; i <= book->charts->top; i++) {
		if (crt_save((PCHART)(book->charts->array[i])) != 1) return -1;
	}

	for (int i = 0; i <= book->drawings->top; i++) {
		if (dr_save((DRAWING *)(book->drawings->array[i])) != 1) return -1;
	}

	lxw_error err;
	int bRetCode = 1;

	for (int i = 0; i <= book->pathManager->contentFiles->top; i++) {
		char FullPath[2048];

		strcpy(FullPath, book->temp_path);
		strcat(FullPath, (char*)book->pathManager->contentFiles->array[i]);

		char* Path = (char*)book->pathManager->contentFiles->array[i];

		bRetCode = zip_addFile(book->zip, FullPath, Path);
	}

	zip_close(book->zip);

	pm_clearTemp(book->pathManager);
	return bRetCode;
}


PWORKSHEET InitWorkSheet(PWORKBOOK book, PWORKSHEET sheet, char* title) {
	sheet->title = title;
	//sheet->SetComments(&m_comments);	      memcpy ? NYI
	sheet->sharedStrings = book->sharedStrings;
	vec_pushback(book->worksheets, sheet);
	return sheet;
}


void wrkbk_saveSharedStrings(PWORKBOOK book) {
	// [- zip/xl/sharedStrings.xml
	if (vec_empty(book->sharedStrings)) return;

	char *retpath = registerFile(book->pathManager, "xl/sharedStrings.xml");
	PXML_WRITER xml = NULL;
	xlsx_initWriter(&xml, retpath);
	free(retpath);

	xlsx_tag(xml, "sst");
	xlsx_AttrString(xml, "xmlns", ns_book);
	xlsx_AttrInt(xml, "count", book->sharedStrings->top+1);
	xlsx_AttrInt(xml, "uniqueCount", book->sharedStrings->top+1); // Consider changing this to a real unique

	for (int i = 0; i <= book->sharedStrings->top; i++) {
		xlsx_tag(xml, "si");
		xlsx_tagOnlyString(xml, "t", (char*)book->sharedStrings->array[i]);
		xlsx_end(xml, "si");
	}

	xlsx_end(xml, "sst");

	xlsx_freeWriter(&xml);
	// zip/xl/sharedStrings.xml -]
}

void wrkbk_saveCore(PWORKBOOK book) {
	{
		time_t t = time(NULL);
		const int MAX_USER_TIME_LENGTH = 32;
		char UserTime[32] = { 0 };
		strftime(UserTime, MAX_USER_TIME_LENGTH, "%Y-%m-%dT%H:%M:%SZ", localtime(&t));

		// [- zip/docProps/core.xml
		char *retpath = registerFile(book->pathManager, "docProps/core.xml");
		PXML_WRITER xml = NULL;
		xlsx_initWriter(&xml, retpath);
		free(retpath);

		xlsx_tag(xml, "cp:coreProperties");
		xlsx_AttrString(xml, "xmlns:cp", ns_cp);
		xlsx_AttrString(xml, "xmlns:dc", ns_dc);

		xlsx_AttrString(xml, "xmlns:dcterms", ns_dcterms);
		xlsx_AttrString(xml, "xmlns:dcmitype", ns_dcmitype);
		xlsx_AttrString(xml, "xmlns:xsi", ns_xsi);

		xlsx_tagOnlyString(xml, "dc:creator", book->UserName);

		xlsx_tagOnlyString(xml, "cp:lastModifiedBy", book->UserName);

		xlsx_tag(xml, "dcterms:created");
		xlsx_AttrString(xml, "xsi:type", xsi_type);
		xlsx_ContentString(xml, UserTime);
		xlsx_end(xml, "dcterms:created");

		xlsx_tag(xml, "dcterms:modified");
		xlsx_AttrString(xml, "xsi:type", xsi_type);
		xlsx_ContentString(xml, UserTime);
		xlsx_end(xml, "dcterms:modified");

		xlsx_end(xml, "cp:coreProperties");
		// zip/docProps/core.xml -]
		xlsx_freeWriter(&xml);
	}
	{
		// [- zip/_rels/.rels
		char *retpath = registerFile(book->pathManager, "_rels/.rels");
		PXML_WRITER xml = NULL;
		xlsx_initWriter(&xml, retpath);
		free(retpath);

		xlsx_tag(xml, "Relationships");
		xlsx_AttrString(xml, "xmlns", ns_relationships);

		const char * Node = "Relationship";

		xlsx_tagL(xml, Node);
		xlsx_AttrString(xml, "Id", "rId3");
		xlsx_AttrString(xml, "Type", type_app);
		xlsx_AttrString(xml, "Target", "docProps/app.xml");
		xlsx_endL(xml);

		xlsx_tagL(xml, Node);
		xlsx_AttrString(xml, "Id", "rId2");
		xlsx_AttrString(xml, "Type", type_core);
		xlsx_AttrString(xml, "Target", "docProps/core.xml");
		xlsx_endL(xml);

		xlsx_tagL(xml, Node);
		xlsx_AttrString(xml, "Id", "rId1");
		xlsx_AttrString(xml, "Type", type_book);
		xlsx_AttrString(xml, "Target", "xl/workbook.xml");
		xlsx_endL(xml);

		xlsx_end(xml, "Relationships");
		// zip/_rels/.rels -]
		xlsx_freeWriter(&xml);
	}
}

void wrkbk_saveContentType(PWORKBOOK book) {
	// [- zip/[Content_Types].xml
	char *retpath = registerFile(book->pathManager, "[Content_Types].xml");
	PXML_WRITER xml = NULL;
	xlsx_initWriter(&xml, retpath);
	free(retpath);

	xlsx_tag(xml, "Types");
	xlsx_AttrString(xml, "xmlns", ns_content_types);

	xlsx_tagL(xml, "Default");
	xlsx_AttrString(xml, "Extension", "rels");
	xlsx_AttrString(xml, "ContentType", content_rels);
	xlsx_endL(xml);

	xlsx_tagL(xml, "Default");
	xlsx_AttrString(xml, "Extension", "xml");
	xlsx_AttrString(xml, "ContentType", content_xml);
	xlsx_endL(xml);

	//AddImagesExtensions( xmlw );

	xlsx_tagL(xml, "Override");
	xlsx_AttrString(xml, "PartName", "/xl/workbook.xml");
	xlsx_AttrString(xml, "ContentType", content_book);
	xlsx_endL(xml);

	for (int i = 0; i <= book->worksheets->top; i++) {
		char buf[64];
		snprintf(buf, 64, "/xl/worksheets/sheet%d%s\0", ((PWORKSHEET)book->worksheets->array[i])->index, ".xml");
		xlsx_tagL(xml, "Override");
		xlsx_AttrString(xml, "PartName", buf);
		xlsx_AttrString(xml, "ContentType", content_sheet);
		xlsx_endL(xml);
	}

	for (int i = 0; i <= book->chartsheets->top; i++) {
		char buf[64];
		snprintf(buf, 64, "/xl/chartsheets/sheet%d%s\0", ((PCHARTSHEET)book->chartsheets->array[i])->index, ".xml");
		xlsx_tagL(xml, "Override");
		xlsx_AttrString(xml, "PartName", buf);
		xlsx_AttrString(xml, "ContentType", content_chartsheet);
		xlsx_endL(xml);
	}

	for (int i = 0; i <= book->drawings->top; i++) {
		char buf[64];
		snprintf(buf, 64, "/xl/drawings/drawing%d.xml", ((DRAWING *)book->drawings->array[i])->index);
		xlsx_tagL(xml, "Override");
		xlsx_AttrString(xml, "PartName", buf);
		xlsx_AttrString(xml, "ContentType", content_drawing);
		xlsx_endL(xml);
	}

	if (!vec_empty(book->sharedStrings)) {
		xlsx_tagL(xml, "Override");
		xlsx_AttrString(xml, "PartName", "/xl/sharedStrings.xml");
		xlsx_AttrString(xml, "ContentType", content_sharedStr);
		xlsx_endL(xml);
	}

	xlsx_tagL(xml, "Override");
	xlsx_AttrString(xml, "PartName", "/xl/theme/theme1.xml");
	xlsx_AttrString(xml, "ContentType", content_theme);
	xlsx_endL(xml);

	xlsx_tagL(xml, "Override");
	xlsx_AttrString(xml, "PartName", "/xl/styles.xml");
	xlsx_AttrString(xml, "ContentType", content_styles);
	xlsx_endL(xml);

	for (int i = 0; i <= book->charts->top; i++)
	{
		char buf[64];
		snprintf(buf, 64, "/xl/charts/chart%d%s\0", ((PCHART)book->charts->array[i])->index, ".xml");
		xlsx_tagL(xml, "Override");
		xlsx_AttrString(xml, "PartName", buf);
		xlsx_AttrString(xml, "ContentType", content_chart);
		xlsx_endL(xml);
	}

	xlsx_tagL(xml, "Override");
	xlsx_AttrString(xml, "PartName", "/docProps/core.xml");
	xlsx_AttrString(xml, "ContentType", content_core);
	xlsx_endL(xml);

	xlsx_tagL(xml, "Override");
	xlsx_AttrString(xml, "PartName", "/docProps/app.xml");
	xlsx_AttrString(xml, "ContentType", content_app);
	xlsx_endL(xml);

	xlsx_end(xml, "Types");
	// zip/[ContentTypes].xml -]
	xlsx_freeWriter(&xml);
}

void wrkbk_saveApp(PWORKBOOK book) {
	// [- zip/docProps/app.xml
	int nSheets = book->worksheets->top + 1;
	int nCharts = book->chartsheets->top + 1;
	int nVectorSize = (nCharts > 0) ? 4 : 2;

	char *retpath = registerFile(book->pathManager, "docProps/app.xml");
	PXML_WRITER xml = NULL;
	xlsx_initWriter(&xml, retpath);
	free(retpath);

	xlsx_tag(xml, "Properties");
	xlsx_AttrString(xml, "xmlns", ns_doc_prop);
	xlsx_AttrString(xml, "xmlns:vt", ns_vt);
	xlsx_tagOnlyString(xml, "Application", "Microsoft Excel");
	xlsx_tagOnlyString(xml, "DocSecurity", "0");
	xlsx_tagOnlyString(xml, "ScaleCrop", "false");

	xlsx_tag(xml, "HeadingPairs");
	xlsx_tag(xml, "vt:vector");
	xlsx_AttrInt(xml, "size", nVectorSize);
	xlsx_AttrString(xml, "baseType", "variant");

	xlsx_tag(xml, "vt:variant");
	xlsx_tagOnlyString(xml, "vt:lpstr", "Worksheets");
	xlsx_end(xml, "vt:variant");

	xlsx_tag(xml, "vt:variant");
	xlsx_tagOnlyInt(xml, "vt:i4", nSheets);
	xlsx_end(xml, "vt:variant");

	if (nCharts > 0) {
		xlsx_tag(xml, "vt:variant");
		xlsx_tagOnlyString(xml, "vt:lpstr", "Diagramms");
		xlsx_end(xml, "vt:variant");

		xlsx_tag(xml, "vt:variant");
		xlsx_tagOnlyInt(xml, "vt:i4", nCharts);
		xlsx_end(xml, "vt:variant");
	}
	xlsx_end(xml, "vt:vector");
	xlsx_end(xml, "HeadingPairs");

	xlsx_tag(xml, "TitlesOfParts");
	xlsx_tag(xml, "vt:vector");
	xlsx_AttrInt(xml, "size", nSheets + nCharts);
	xlsx_AttrString(xml, "baseType", "lpstr");

	for (int i = 0; i < nSheets; i++) {
		xlsx_tagOnlyString(xml, "vt:lpstr", ((PWORKSHEET)book->worksheets->array[i])->title);
	}

	for (int i = 0; i < nCharts; i++) {
		xlsx_tagOnlyString(xml, "vt:lpstr", ((PCHARTSHEET)book->chartsheets->array[i])->chart->title);
	}

	xlsx_end(xml, "vt:vector");
	xlsx_end(xml, "TitlesOfParts");

	xlsx_tagOnlyString(xml, "Company", "Henschen");
	xlsx_tagOnlyString(xml, "LinksUpToDate", "false");
	xlsx_tagOnlyString(xml, "SharedDoc", "false");
	xlsx_tagOnlyString(xml, "HyperlinksChanged", "false");
	xlsx_tagOnlyString(xml, "AppVersion", "14.03");         //Microsoft Excel 2010
	xlsx_end(xml, "Properties");
	// zip/docProps/app.xml -]
	xlsx_freeWriter(&xml);
}

void wrkbk_saveTheme(PWORKBOOK book) {
	// [- zip/xl/theme/theme1.xml

	char *retpath = registerFile(book->pathManager, "xl/theme/theme1.xml");
	PXML_WRITER xml = NULL;
	xlsx_initWriter(&xml, retpath);
	free(retpath);

	xlsx_tag(xml, "a:theme");
	xlsx_AttrString(xml, "xmlns:a", ns_a);
	xlsx_AttrString(xml, "name", "Office Theme");

	xlsx_tag(xml, "a:themeElements");

	xlsx_tag(xml, "a:clrScheme");
	xlsx_AttrString(xml, "name", "Office");

	xlsx_tag(xml, "a:dk1");
	xlsx_tagL(xml, "a:sysClr");
	xlsx_AttrString(xml, "val", "windowText");
	xlsx_AttrString(xml, "lastClr", "000000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL); //Blank end

	xlsx_tag(xml, "a:lt1");
	xlsx_tagL(xml, "a:sysClr");
	xlsx_AttrString(xml, "val", "window");
	xlsx_AttrString(xml, "lastClr", "FFFFFF");
	xlsx_endL(xml);
	xlsx_end(xml, NULL); //Blank end

	xlsx_tag(xml, "a:dk2");
	xlsx_tagL(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "1F497D");
	xlsx_endL(xml);
	xlsx_end(xml, NULL); //Blank end

	xlsx_tag(xml, "a:lt2");
	xlsx_tagL(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "EEECE1");
	xlsx_endL(xml);
	xlsx_end(xml, NULL); //Blank end

	xlsx_tag(xml, "a:accent1");
	xlsx_tagL(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "4F81BD");
	xlsx_endL(xml);
	xlsx_end(xml, NULL); //Blank end

	xlsx_tag(xml, "a:accent2");
	xlsx_tagL(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "C0504D");
	xlsx_endL(xml);
	xlsx_end(xml, NULL); //Blank end

	xlsx_tag(xml, "a:accent3");
	xlsx_tagL(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "9BBB59");
	xlsx_endL(xml);
	xlsx_end(xml, NULL); //Blank end

	xlsx_tag(xml, "a:accent4");
	xlsx_tagL(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "8064A2");
	xlsx_endL(xml);
	xlsx_end(xml, NULL); //Blank end

	xlsx_tag(xml, "a:accent5");
	xlsx_tagL(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "4BACC6");
	xlsx_endL(xml);
	xlsx_end(xml, NULL); //Blank end

	xlsx_tag(xml, "a:accent6");
	xlsx_tagL(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "F79646");
	xlsx_endL(xml);
	xlsx_end(xml, NULL); //Blank end

	xlsx_tag(xml, "a:hlink");
	xlsx_tagL(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "0000FF");
	xlsx_endL(xml);
	xlsx_end(xml, NULL); //Blank end

	xlsx_tag(xml, "a:folHlink");
	xlsx_tagL(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "800080");
	xlsx_endL(xml);
	xlsx_end(xml, NULL); //Blank end

	xlsx_end(xml, "a:clrScheme");

	const char * szAFont = "a:font";
	const char * szScript = "script";
	const char * szTypeface = "typeface";
	xlsx_tag(xml, "a:fontScheme");
	xlsx_AttrString(xml, "name", "Office");
	for (int i = 0; i < 2; i++) {
		if (i == 0) xlsx_tag(xml, "a:majorFont");
		else xlsx_tag(xml, "a:minorFont");

		xlsx_tagL(xml, "a:latin");
		xlsx_AttrString(xml, szTypeface, "Cambria");
		xlsx_endL(xml);

		xlsx_tagL(xml, "a:ea");
		xlsx_AttrString(xml, szTypeface, "");
		xlsx_endL(xml);

		xlsx_tagL(xml, "a:cs");
		xlsx_AttrString(xml, szTypeface, "");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Arab");
		xlsx_AttrString(xml, szTypeface, "Times New Roman");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Herb");
		xlsx_AttrString(xml, szTypeface, "Times New Roman");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Thai");
		xlsx_AttrString(xml, szTypeface, "Tahoma");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Ethi");
		xlsx_AttrString(xml, szTypeface, "Nyala");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Beng");
		xlsx_AttrString(xml, szTypeface, "Vrinda");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Gujr");
		xlsx_AttrString(xml, szTypeface, "Shruti");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Khmr");
		xlsx_AttrString(xml, szTypeface, "MoolBoran");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Knda");
		xlsx_AttrString(xml, szTypeface, "Tunga");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Guru");
		xlsx_AttrString(xml, szTypeface, "Raavi");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Cans");
		xlsx_AttrString(xml, szTypeface, "Euphemia");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Cher");
		xlsx_AttrString(xml, szTypeface, "Plantagenet Cherokee");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Yiii");
		xlsx_AttrString(xml, szTypeface, "Microsoft Yi Baiti");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Tibt");
		xlsx_AttrString(xml, szTypeface, "Microsoft Himalaya");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Thaa");
		xlsx_AttrString(xml, szTypeface, "MV Boli");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Deva");
		xlsx_AttrString(xml, szTypeface, "Mangal");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Telu");
		xlsx_AttrString(xml, szTypeface, "Gautami");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Taml");
		xlsx_AttrString(xml, szTypeface, "Latha");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Syrc");
		xlsx_AttrString(xml, szTypeface, "Estrangelo Edessa");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Orya");
		xlsx_AttrString(xml, szTypeface, "Kalinga");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Mlym");
		xlsx_AttrString(xml, szTypeface, "Kartika");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Laoo");
		xlsx_AttrString(xml, szTypeface, "DokChampa");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Sinh");
		xlsx_AttrString(xml, szTypeface, "Iskoola Pota");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Mong");
		xlsx_AttrString(xml, szTypeface, "Mongolian Baiti");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Viet");
		xlsx_AttrString(xml, szTypeface, "Times New Roman");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Uigh");
		xlsx_AttrString(xml, szTypeface, "Microsoft Uighur");
		xlsx_endL(xml);

		xlsx_tagL(xml, szAFont);
		xlsx_AttrString(xml, szScript, "Geor");
		xlsx_AttrString(xml, szTypeface, "Sylfaen");
		xlsx_endL(xml);

		xlsx_end(xml, NULL); //For "a:majorFont" and "a:minorFont"
	}
	xlsx_end(xml, "a:fontScheme");

	xlsx_tag(xml, "a:fmtScheme");
	xlsx_AttrString(xml, "name", "Office");

	xlsx_tag(xml, "a:fillStyleLst");

	xlsx_tag(xml, "a:solidFill");
	xlsx_tagL(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);

	xlsx_tag(xml, "a:gradFill");
	xlsx_AttrString(xml, "rotWithShape", "1");

	xlsx_tag(xml, "a:gsLst");

	/**/
	xlsx_tag(xml, "a:gs");
	xlsx_AttrString(xml, "pos", "0");
	xlsx_tag(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");

	/**/
	xlsx_tagL(xml, "a:tint");
	xlsx_AttrString(xml, "val", "50000");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:satMod");
	xlsx_AttrString(xml, "val", "300000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);
	/**/
	xlsx_tag(xml, "a:gs");
	xlsx_AttrString(xml, "pos", "35000");
	xlsx_tag(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	/**/
	xlsx_tagL(xml, "a:tint");
	xlsx_AttrString(xml, "val", "37000");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:satMod");
	xlsx_AttrString(xml, "val", "300000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);
	/**/
	xlsx_tag(xml, "a:gs");
	xlsx_AttrString(xml, "pos", "100000");
	xlsx_tag(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	/**/
	xlsx_tagL(xml, "a:tint");
	xlsx_AttrString(xml, "val", "15000");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:satMod");
	xlsx_AttrString(xml, "val", "350000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);

	xlsx_end(xml, "a:gsLst");
	xlsx_tagL(xml, "a:lin");
	xlsx_AttrString(xml, "ang", "16200000");
	xlsx_AttrString(xml, "scaled", "1");
	xlsx_endL(xml);

	xlsx_end(xml, "a:gradFill");

	xlsx_tag(xml, "a:gradFill");
	xlsx_AttrString(xml, "rotWithShape", "1");

	xlsx_tag(xml, "a:gsLst");

	/**/
	xlsx_tag(xml, "a:gs");
	xlsx_AttrString(xml, "pos", "0");
	xlsx_tag(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	/**/
	xlsx_tagL(xml, "a:shade");
	xlsx_AttrString(xml, "val", "51000");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:satMod");
	xlsx_AttrString(xml, "val", "130000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);
	/**/
	xlsx_tag(xml, "a:gs");
	xlsx_AttrString(xml, "pos", "80000");
	xlsx_tag(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	/**/
	xlsx_tagL(xml, "a:shade");
	xlsx_AttrString(xml, "val", "93000");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:satMod");
	xlsx_AttrString(xml, "val", "130000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);
	/**/
	xlsx_tag(xml, "a:gs");
	xlsx_AttrString(xml, "pos", "100000");
	xlsx_tag(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	/**/
	xlsx_tagL(xml, "a:shade");
	xlsx_AttrString(xml, "val", "94000");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:satMod");
	xlsx_AttrString(xml, "val", "135000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);

	xlsx_end(xml, "a:gsLst");

	xlsx_tagL(xml, "a:lin");
	xlsx_AttrString(xml, "ang", "16200000");
	xlsx_AttrString(xml, "scaled", "0");
	xlsx_endL(xml);

	xlsx_end(xml, "a:gradFill");

	xlsx_end(xml, "a:fillStyleLst");

	xlsx_tag(xml, "a:lnStyleLst");
	xlsx_tag(xml, "a:ln");
	xlsx_AttrString(xml, "w", "9525");
	xlsx_AttrString(xml, "cap", "flat");
	xlsx_AttrString(xml, "cmpd", "sng");
	xlsx_AttrString(xml, "algn", "ctr");

	/**/
	xlsx_tag(xml, "a:solidFill");
	xlsx_tag(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	/**/
	xlsx_tagL(xml, "a:shade");
	xlsx_AttrString(xml, "val", "95000");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:satMod");
	xlsx_AttrString(xml, "val", "105000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);
	/**/
	xlsx_tagL(xml, "a:prstDash");
	xlsx_AttrString(xml, "val", "solid");
	xlsx_endL(xml);
	xlsx_end(xml, "a:ln");
	xlsx_tag(xml, "a:ln");
	xlsx_AttrString(xml, "w", "25400");
	xlsx_AttrString(xml, "cap", "flat");
	xlsx_AttrString(xml, "cmpd", "sng");
	xlsx_AttrString(xml, "algn", "ctr");
	/**/
	xlsx_tag(xml, "a:solidFill");
	xlsx_tagL(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	/**/
	xlsx_tagL(xml, "a:prstDash");
	xlsx_AttrString(xml, "val", "solid");
	xlsx_endL(xml);
	xlsx_end(xml, "a:ln");

	xlsx_tag(xml, "a:ln");
	xlsx_AttrString(xml, "w", "38100");
	xlsx_AttrString(xml, "cap", "flat");
	xlsx_AttrString(xml, "cmpd", "sng");
	xlsx_AttrString(xml, "algn", "ctr");
	/**/
	xlsx_tag(xml, "a:solidFill");
	xlsx_tagL(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	/**/
	xlsx_tagL(xml, "a:prstDash");
	xlsx_AttrString(xml, "val", "solid");
	xlsx_endL(xml);

	xlsx_end(xml, "a:ln");
	xlsx_end(xml, "a:lnStyleLst");

	xlsx_tag(xml, "a:effectStyleLst");

	xlsx_tag(xml, "a:effectStyle");
	xlsx_tag(xml, "a:effectLst");

	/**/
	xlsx_tag(xml, "a:outerShdw");
	xlsx_AttrString(xml, "blurRad", "40000");
	xlsx_AttrString(xml, "dist", "20000");
	xlsx_AttrString(xml, "dir", "5400000");
	xlsx_AttrString(xml, "rotWithShape", "0");
	/**/
	xlsx_tag(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "000000");
	xlsx_tagL(xml, "a:alpha");
	xlsx_AttrString(xml, "val", "38000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);

	xlsx_end(xml, "a:effectLst");
	xlsx_end(xml, "a:effectStyle");

	xlsx_tag(xml, "a:effectStyle");
	xlsx_tag(xml, "a:effectLst");
	/**/
	xlsx_tag(xml, "a:outerShdw");
	xlsx_AttrString(xml, "blurRad", "40000");
	xlsx_AttrString(xml, "dist", "23000");
	xlsx_AttrString(xml, "dir", "5400000");
	xlsx_AttrString(xml, "rotWithShape", "0");
	/**/
	xlsx_tag(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "000000");
	xlsx_tagL(xml, "a:alpha");
	xlsx_AttrString(xml, "val", "35000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);

	xlsx_end(xml, "a:effectLst");
	xlsx_end(xml, "a:effectStyle");
	xlsx_tag(xml, "a:effectStyle");
	xlsx_tag(xml, "a:effectLst");
	/**/
	xlsx_tag(xml, "a:outerShdw");
	xlsx_AttrString(xml, "blurRad", "40000");
	xlsx_AttrString(xml, "dist", "23000");
	xlsx_AttrString(xml, "dir", "5400000");
	xlsx_AttrString(xml, "rotWithShape", "0");
	/**/
	xlsx_tag(xml, "a:srgbClr");
	xlsx_AttrString(xml, "val", "000000");
	xlsx_tagL(xml, "a:alpha");
	xlsx_AttrString(xml, "val", "35000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);

	xlsx_end(xml, "a:effectLst");
	xlsx_tag(xml, "a:scene3d");

	/**/
	xlsx_tag(xml, "a:camera");
	xlsx_AttrString(xml, "prst", "orthographicFront");
	/**/
	xlsx_tagL(xml, "a:rot");
	xlsx_AttrString(xml, "lat", "0");
	xlsx_AttrString(xml, "lon", "0");
	xlsx_AttrString(xml, "rev", "0");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	/**/
	xlsx_tag(xml, "a:lightRig");
	xlsx_AttrString(xml, "rig", "threePt");
	xlsx_AttrString(xml, "dir", "t");
	/**/
	xlsx_tagL(xml, "a:rot");
	xlsx_AttrString(xml, "lat", "0");
	xlsx_AttrString(xml, "lon", "0");
	xlsx_AttrString(xml, "rev", "1200000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, "a:scene3d");
	xlsx_tag(xml, "a:sp3d");
	xlsx_tag(xml, "a:bevelT");
	xlsx_AttrString(xml, "w", "63500");
	xlsx_AttrString(xml, "h", "25400");
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);
	xlsx_end(xml, "a:effectStyle");
	xlsx_end(xml, "a:effectStyleLst");

	xlsx_tag(xml, "a:bgFillStyleLst");

	xlsx_tag(xml, "a:solidFill");
	xlsx_tagL(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_tag(xml, "a:gradFill");
	xlsx_AttrString(xml, "rotWithShape", "1");
	xlsx_tag(xml, "a:gsLst");
	/**/
	xlsx_tag(xml, "a:gs");
	xlsx_AttrString(xml, "pos", "0");
	xlsx_tag(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	/**/
	xlsx_tagL(xml, "a:tint");
	xlsx_AttrString(xml, "val", "40000");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:satMod");
	xlsx_AttrString(xml, "val", "350000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);
	/**/
	xlsx_tag(xml, "a:gs");
	xlsx_AttrString(xml, "pos", "40000");
	xlsx_tag(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	/**/
	xlsx_tagL(xml, "a:tint");
	xlsx_AttrString(xml, "val", "45000");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:shade");
	xlsx_AttrString(xml, "val", "99000");
	xlsx_endL(xml);
	/**/
	xlsx_tagL(xml, "a:satMod");
	xlsx_AttrString(xml, "val", "350000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);
	/**/
	xlsx_tag(xml, "a:gs");
	xlsx_AttrString(xml, "pos", "100000");
	xlsx_tag(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	/**/
	xlsx_tagL(xml, "a:shade");
	xlsx_AttrString(xml, "val", "20000");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:satMod");
	xlsx_AttrString(xml, "val", "255000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);
	xlsx_end(xml, "a:gsLst");
	xlsx_tag(xml, "a:path");
	xlsx_AttrString(xml, "path", "circle");
	/**/
	xlsx_tagL(xml, "a:fillToRect");
	xlsx_AttrString(xml, "l", "50000");
	xlsx_AttrString(xml, "t", "-80000");
	xlsx_AttrString(xml, "r", "50000");
	xlsx_AttrString(xml, "b", "180000");
	xlsx_endL(xml);
	xlsx_end(xml, "a:path");
	xlsx_end(xml, "a:gradFill");
	xlsx_tag(xml, "a:gradFill");
	xlsx_AttrString(xml, "rotWithShape", "1");
	xlsx_tag(xml, "a:gsLst");
	/**/
	xlsx_tag(xml, "a:gs");
	xlsx_AttrString(xml, "pos", "0");
	xlsx_tag(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	/**/
	xlsx_tagL(xml, "a:tint");
	xlsx_AttrString(xml, "val", "80000");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:satMod");
	xlsx_AttrString(xml, "val", "300000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);
	/**/
	xlsx_tag(xml, "a:gs");
	xlsx_AttrString(xml, "pos", "100000");
	xlsx_tag(xml, "a:schemeClr");
	xlsx_AttrString(xml, "val", "phClr");
	/**/
	xlsx_tagL(xml, "a:shade");
	xlsx_AttrString(xml, "val", "30000");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:satMod");
	xlsx_AttrString(xml, "val", "200000");
	xlsx_endL(xml);
	xlsx_end(xml, NULL);
	xlsx_end(xml, NULL);

	xlsx_end(xml, "a:gsLst");

	xlsx_tag(xml, "a:path");
	xlsx_AttrString(xml, "path", "circle");
	/**/
	xlsx_tagL(xml, "a:fillToRect");
	xlsx_AttrString(xml, "l", "50000");
	xlsx_AttrString(xml, "t", "50000");
	xlsx_AttrString(xml, "r", "50000");
	xlsx_AttrString(xml, "b", "50000");
	xlsx_endL(xml);

	xlsx_end(xml, "a:path");
	xlsx_end(xml, "a:gradFill");

	xlsx_end(xml, "a:bgFillStyleLst");

	xlsx_end(xml, "a:fmtScheme");

	xlsx_end(xml, "a:themeElements");
	xlsx_tagL(xml, "a:objectDefaults");
	xlsx_endL(xml);
	xlsx_tagL(xml, "a:extraClrSchemeLst");
	xlsx_endL(xml);
	xlsx_end(xml, "a:theme");
	// zip/xl/theme/theme1.xml -]
	xlsx_freeWriter(&xml);
}


void wrkbk_saveStyles(PWORKBOOK book) {
	// [- zip/xl/styles.xml

	char *retpath = registerFile(book->pathManager, "xl/styles.xml");
	PXML_WRITER xml = NULL;
	xlsx_initWriter(&xml, retpath);
	free(retpath);

	xlsx_tag(xml, "styleSheet");
	xlsx_AttrString(xml, "xmlns", ns_book);
	xlsx_AttrString(xml, "xmlns:mc", ns_mc);
	xlsx_AttrString(xml, "mc:Ignorable", "x14ac");
	xlsx_AttrString(xml, "xmlns:x14ac", ns_x14ac);

	AddNumberFormats(book, xml);
	AddFonts(book, xml);
	//  AddFills( xmlw );
	//  AddBorders( xmlw );

	xlsx_tag(xml, "cellStyleXfs");
	xlsx_AttrString(xml, "count", "1");
	xlsx_tagL(xml, "xf");
	xlsx_AttrString(xml, "numFmtId", "0");
	xlsx_AttrString(xml, "fontId", "0");
	xlsx_AttrString(xml, "fillId", "0");
	xlsx_AttrString(xml, "borderId", "0");
	xlsx_endL(xml);
	xlsx_end(xml, "cellStyleXfs");

	xlsx_tag(xml, "cellXfs");

	PVECTOR styleIndexes = vec_init();
	memcpy(styleIndexes->array, book->styleList->styleIndexes->array, sizeof(book->styleList->styleIndexes->array));
	styleIndexes->top = book->styleList->styleIndexes->top;
	PVECTOR styleAligns = vec_init();
	memcpy(styleAligns->array, book->styleList->stylePos->array, sizeof(book->styleList->stylePos->array));
	styleAligns->top = book->styleList->stylePos->top;
	assert(styleIndexes->top == styleAligns->top);

	for (int i = 0; i <= book->styleList->styleIndexes->top; i++)
	{
		PVECTOR index = vec_init();
		memcpy(index->array, book->styleList->styleIndexes->array[i], sizeof(book->styleList->styleIndexes->array[i]));

		PTRIPLET align;
		align = book->styleList->stylePos->array[i];

		xlsx_tag(xml, "xf");
		xlsx_AttrString(xml, "numFmtId", index->array[STYLE_LINK_NUM_FORMAT]);
		xlsx_AttrString(xml, "fontId", index->array[STYLE_LINK_FONT]);
		xlsx_AttrString(xml, "fillId", index->array[STYLE_LINK_FILL]);
		xlsx_AttrString(xml, "borderId", index->array[STYLE_LINK_BORDER]);

		if (index->array[STYLE_LINK_FONT] != 0)        xlsx_AttrString(xml, "applyFont", "1");
		if (index->array[STYLE_LINK_FILL] != 0)        xlsx_AttrString(xml, "applyFill", "1");
		if (index->array[STYLE_LINK_BORDER] != 0)      xlsx_AttrString(xml, "applyBorder", "1");
		if (index->array[STYLE_LINK_NUM_FORMAT] != 0)  xlsx_AttrString(xml, "applyNumberFormat", "1");

		if ((align->t1 != ALIGN_H_NONE) || (align->t2 != ALIGN_V_NONE) || align->t3) {
			xlsx_tagL(xml, "alignment");
			switch (align->t1) {
			case ALIGN_H_LEFT:   xlsx_AttrString(xml, "horizontal", "left");      break;
			case ALIGN_H_CENTER:   xlsx_AttrString(xml, "horizontal", "center");    break;
			case ALIGN_H_RIGHT:   xlsx_AttrString(xml, "horizontal", "right");     break;
			case ALIGN_H_NONE:
				/*default:*/            break;
			}
			switch (align->t2)
			{
			case ALIGN_V_BOTTOM:   xlsx_AttrString(xml, "vertical", "bottom");  break;
			case ALIGN_V_CENTER:   xlsx_AttrString(xml, "vertical", "center");  break;
			case ALIGN_V_TOP:   xlsx_AttrString(xml, "vertical", "top");     break;
			case ALIGN_V_NONE:
				/*default:*/            break;
			}
			if (align->t3) {
				xlsx_AttrString(xml, "wrapText", "1");
			}
			xlsx_endL(xml);
		}
		xlsx_end(xml, "xf");
		vec_free(index);
	}


	xlsx_end(xml, "cellXfs");

	xlsx_tag(xml, "cellStyles");
	xlsx_AttrString(xml, "count", "1");

	xlsx_tagL(xml, "cellStyle");
	xlsx_AttrString(xml, "name", "Normal");
	xlsx_AttrString(xml, "xfId", "0");
	xlsx_AttrString(xml, "builtinId", "0");
	xlsx_endL(xml);

	xlsx_end(xml, "cellStyles");

	xlsx_tagL(xml, "dxfs");
	xlsx_AttrString(xml, "count", "0");
	xlsx_endL(xml);

	xlsx_tagL(xml, "tableStyles");
	xlsx_AttrString(xml, "count", "0");
	xlsx_AttrString(xml, "defaultTableStyle", "TableStyleMedium2");

	xlsx_AttrString(xml, "defaultPivotStyle", "PivotStyleLight16");
	xlsx_endL(xml);

	xlsx_tag(xml, "extLst");

	xlsx_tag(xml, "ext");
	xlsx_AttrString(xml, "uri", "{EB79DEF2-80B8-43e5-95BD-54CBDDF9020C}");
	xlsx_AttrString(xml, "xmlns:x14", ns_x14);

	xlsx_tagL(xml, "x14:slicerStyles");
	xlsx_AttrString(xml, "defaultSlicerStyle", "SlicerStyleLight1");
	xlsx_endL(xml);

	xlsx_end(xml, "ext");

	xlsx_end(xml, "extLst");

	xlsx_end(xml, "styleSheet");
	// zip/xl/styles.xml -]
	xlsx_freeWriter(&xml);

	vec_free(styleIndexes);
	vec_free(styleAligns);
}



void AddNumberFormats(PWORKBOOK book, PXML_WRITER xml) {
	PVECTOR nums = vec_init();
	memcpy(nums->array, book->styleList->nums->array, sizeof(book->styleList->nums->array));
	nums->top = book->styleList->nums->top;


	int built_in_formats = 0;
	for (int i = 0; i <= nums->top; i++) {
		if (((PNUM_FORMAT)nums->array[i])->id < BUILT_IN_STYLES_NUMBER) {
			built_in_formats++;
		}
	}

	if ((nums->top + 1) - built_in_formats == 0) return;

	xlsx_tag(xml, "numFmts");
	xlsx_AttrInt(xml, "count", (nums->top + 1) - built_in_formats);
	for (int i = 0; i <= nums->top; i++) {
		if (((PNUM_FORMAT)nums->array[i])->id < BUILT_IN_STYLES_NUMBER) continue;
		char buf[64];
		GetFormatCodeString(buf, book, nums->array[i]);
		xlsx_tag(xml, "numFmt");
		xlsx_AttrInt(xml, "numFmtId", ((PNUM_FORMAT)nums->array[i])->id);
		xlsx_AttrString(xml, "formatCode", buf);
		xlsx_end(xml, "");
	}
	xlsx_end(xml, "numFmts");
}


void GetFormatCodeString(char retValBuf[], PWORKBOOK book, PNUM_FORMAT fmt) {
	if (fmt->format != "") {
		snprintf(retValBuf, 64, "%s", fmt->format);
		return;
	}

	int addNegative = 1;
	int addZero = 1;

	if (fmt->positiveColor != NUMSTYLE_COLOR_DEFAULT) {
		addNegative = addZero = 0;
	}

	if (fmt->negativeColor != NUMSTYLE_COLOR_DEFAULT) {
		addNegative = 0;
	}

	if (fmt->zeroColor != NUMSTYLE_COLOR_DEFAULT) {
		addZero = 0;
	}

	char* thousandPrefix;
	if (fmt->showThousandsSeparator) thousandPrefix = "#,##";

	char* currency = "$";

	char resCode[64];
	char* affix = "";
	char digits[16];
	char bufPos[64];
	char bufNeg[64];
	if (fmt->numberOfDigitsAfterPoint != 0) {
		snprintf(digits, 16, "0.%i", fmt->numberOfDigitsAfterPoint);
	}

	switch (fmt->numberStyle) {
	case NUMSTYLE_EXPONENTIAL:
		affix = "E+00";
		break;
	case NUMSTYLE_PERCENTAGE:
		affix = "%";
		break;
	case NUMSTYLE_FINANCIAL:
		//_-* #,##0.00"$"_-;\-* #,##0.00"$"_-;_-* "-"??"$"_-;_-@_-
		GetFormatCodeColor(bufPos, book, fmt->positiveColor);
		GetFormatCodeColor(bufNeg, book, fmt->negativeColor);
		snprintf(retValBuf, 256, "%s_-* %s%s%s_-;%s\\-* %s%s%s_-;_-* &quot;-&quot;??%s_-;_-@_-\0",
			bufPos, thousandPrefix, digits, currency, bufNeg, thousandPrefix, digits, currency, currency);
		break;
	case NUMSTYLE_MONEY:
		strcat(affix, currency);
		break;
	case NUMSTYLE_DATETIME:
		snprintf(resCode, 64, "yyyy.mm.dd hh:mm:ss\0");
		break;
	case NUMSTYLE_DATE:
		snprintf(resCode, 64, "yyyy.mm.dd\0");
		break;
	case NUMSTYLE_TIME:
		snprintf(resCode, 64, "hh:mm:ss\0");
		break;

	case NUMSTYLE_GENERAL:
	case NUMSTYLE_NUMERIC:
		/*default:*/
		affix = "";
		break;
	}

	if (fmt->numberStyle == NUMSTYLE_GENERAL || fmt->numberStyle == NUMSTYLE_NUMERIC ||
		fmt->numberStyle == NUMSTYLE_EXPONENTIAL || fmt->numberStyle == NUMSTYLE_PERCENTAGE ||
		fmt->numberStyle == NUMSTYLE_MONEY)
	{
		memset(bufPos, 0, 64);

		GetFormatCodeColor(bufPos, book, fmt->positiveColor);
		snprintf(retValBuf, 64, "%s%s%s%s\0", bufPos, thousandPrefix, digits, affix);
		if (addNegative) {
			GetFormatCodeColor(bufNeg, book, fmt->negativeColor);
			snprintf(resCode, 64, ";%s\\-%s%s%s", bufNeg, thousandPrefix, digits, affix);
			strcat(retValBuf, resCode);
		}
		if (addZero) {
			memset(bufNeg, 0, 64);
			GetFormatCodeColor(bufNeg, book, fmt->zeroColor);
			if (addNegative == 1)
				snprintf(resCode, 64, ";;%s0%s/0", bufNeg, affix);
			else
				snprintf(resCode, 64, ";%s0%s/0", bufNeg, affix);
			strcat(retValBuf, resCode);
		}
	}
}


void GetFormatCodeColor(char retValBuf[], PWORKBOOK book, ENumericStyleColor color) {
	char* retVal = "";

	switch (color) {
	case NUMSTYLE_COLOR_BLACK:	retVal = "[BLACK]";
	case NUMSTYLE_COLOR_GREEN:	retVal = "[Green]";
	case NUMSTYLE_COLOR_WHITE:	retVal = "[White]";
	case NUMSTYLE_COLOR_BLUE:	retVal = "[Blue]";
	case NUMSTYLE_COLOR_MAGENTA:   retVal = "[Magenta]";
	case NUMSTYLE_COLOR_YELLOW:	retVal = "[Yellow]";
	case NUMSTYLE_COLOR_CYAN:	retVal = "[Cyan]";
	case NUMSTYLE_COLOR_RED:	retVal = "[Red]";

	case NUMSTYLE_COLOR_DEFAULT:
		/*default:*/                        retVal = "";
	}
	snprintf(retValBuf, 64, "%s\0", retVal);
}


void AddFonts(PWORKBOOK book, PXML_WRITER xml)
{
	const int defaultCharset = 204;
	xlsx_tag(xml, "fonts");
	xlsx_AttrInt(xml, "count", book->styleList->fonts->top + 1);
	for (int i = 0; i <= book->styleList->fonts->top; i++) {
		xlsx_tag(xml, "font");
		AddFontInfo(xml, book->styleList->fonts->array[i], "name", defaultCharset);
		xlsx_end(xml, "font");
	}
	xlsx_end(xml, "fonts");
}


void AddFontInfo(PXML_WRITER xml, PFONT font, char * FontTagName, int Charset)
{
	int attributes = font->attributes;
	if (attributes & FONT_BOLD) {
		xlsx_tagL(xml, "b");
		xlsx_endL(xml);
	}
	if (attributes & FONT_ITALIC) {
		xlsx_tagL(xml, "i");
		xlsx_endL(xml);
	}
	if (attributes & FONT_UNDERLINED) {
		xlsx_tagL(xml, "u");
		xlsx_endL(xml);
	}
	if (attributes & FONT_STRIKE) {
		xlsx_tagL(xml, "strike");
		xlsx_endL(xml);
	}
	if (attributes & FONT_OUTLINE) {
		xlsx_tagL(xml, "outline");
		xlsx_endL(xml);
	}
	if (attributes & FONT_SHADOW) {
		xlsx_tagL(xml, "shadow");
		xlsx_endL(xml);
	}
	if (attributes & FONT_CONDENSE) {
		xlsx_tagL(xml, "condense");
		xlsx_endL(xml);
	}
	if (attributes & FONT_EXTEND) {
		xlsx_tagL(xml, "extend");
		xlsx_endL(xml);
	}

	xlsx_tagL(xml, "sz");
	xlsx_AttrInt(xml, "val", font->size);
	xlsx_endL(xml);

	xlsx_tagL(xml, FontTagName);
	xlsx_AttrString(xml, "val", font->name);
	xlsx_endL(xml);

	xlsx_tagL(xml, "charset");
	xlsx_AttrInt(xml, "val", Charset);
	xlsx_endL(xml);

	if (font->theme || (font->color == "")) {
		xlsx_tagL(xml, "color");
		xlsx_AttrString(xml, "theme", "1");
		xlsx_endL(xml);
	}
	else {
		xlsx_tagL(xml, "color");
		xlsx_AttrString(xml, "rgb", font->color);
		xlsx_endL(xml);
	}
}

PWORKSHEET wrkbk_addSheet(PWORKBOOK book, char * title) {
	PWORKSHEET sheet = NULL;
	sheet = ws_init(sheet, NULL, book->pathManager, book->sheetId++, 0, 0, NULL);
	return InitWorkSheet(book, sheet, title);
}

PCHART wrkbk_addChart(PWORKBOOK book, PWORKSHEET sheet, DRAWINGPOINT TopLeft, DRAWINGPOINT BottomRight, EChartTypes type) {
	PCHART chart = NULL;
	chart = crt_init(chart, book->charts->top + 2, type, book->pathManager);
	vec_pushback(book->charts, chart);
	dr_appendChart(sheet->drawing, chart, TopLeft, BottomRight);
	return chart;
}

PCHARTSHEET wrkbk_addChartSheet(PWORKBOOK book, const char * title, EChartTypes type) {
	PCHART chart = NULL;
	chart = crt_init(chart, book->charts->top + 2, type, book->pathManager);
	chart->title = title;
	//crt_addTitle(chart, title, chart->index, 1);
	vec_pushback(book->charts, chart);

	DRAWINGPOINT drpnt;
	drpnt.col = 0;
	drpnt.colOff = 0;
	drpnt.row = 0;
	drpnt.rowOff = 0;
	DRAWING * drawing = wrkbk_createDrawing(book);
	dr_appendChart(drawing, chart, drpnt, drpnt);

	PCHARTSHEET chartsheet = NULL;
	chartsheet = crtsht_init(chartsheet, book->sheetId++, chart, drawing, book->pathManager);
	 
	vec_pushback(book->chartsheets, chartsheet);

	return chartsheet;
}

DRAWING * wrkbk_createDrawing(PWORKBOOK book) {
	DRAWING * drawing = NULL;
	drawing = dr_init(drawing, book->drawings->top + 2, book->pathManager);
	vec_pushback(book->drawings, drawing);
	return drawing;
}

void wrkbk_saveWorkbook(PWORKBOOK book) {
	char szId[16] = { 0 };
	{
		// [- zip/xl/_rels/workbook.xml.rels

		char *retpath = registerFile(book->pathManager, "xl/_rels/workbook.xml.rels");
		PXML_WRITER xml = NULL;
		xlsx_initWriter(&xml, retpath);
		free(retpath);
		size_t id = book->sheetId;

		xlsx_tag(xml, "Relationships");
		xlsx_AttrString(xml, "xmlns", ns_relationships);

		sprintf(szId, "rId%zu", id++);
		xlsx_tagL(xml, "Relationship");
		xlsx_AttrString(xml, "Id", szId);
		xlsx_AttrString(xml, "Type", type_style);
		xlsx_AttrString(xml, "Target", "styles.xml");
		xlsx_endL(xml);

		sprintf(szId, "rId%zu", id++);
		xlsx_tagL(xml, "Relationship");
		xlsx_AttrString(xml, "Id", szId);
		xlsx_AttrString(xml, "Type", type_theme);
		xlsx_AttrString(xml, "Target", "theme/theme1.xml");
		xlsx_endL(xml);

		int bFormula = 0;
		for (int i = 0; i <= book->worksheets->top; i++) {
			sprintf(szId, "rId%zu", ((PWORKSHEET)book->worksheets->array[i])->index);
			char buf[64];
			snprintf(buf, 64, "worksheets/sheet%d%s\0", ((PWORKSHEET)book->worksheets->array[i])->index, ".xml");
			xlsx_tagL(xml, "Relationship");
			xlsx_AttrString(xml, "Id", szId);
			xlsx_AttrString(xml, "Type", type_sheet);
			xlsx_AttrString(xml, "Target", buf);
			xlsx_endL(xml);
			if (((PWORKSHEET)book->worksheets->array[i])->withFormula != 0) bFormula = 1;
		}
		for (int i = 0; i <= book->chartsheets->top; i++) {
			sprintf(szId, "rId%zu", ((PCHARTSHEET)book->chartsheets->array[i])->index);
			char buf[64];
			snprintf(buf, 64, "chartsheets/sheet%d%s\0", (int)((PCHARTSHEET)book->chartsheets->array[i])->index, ".xml");
			xlsx_tagL(xml, "Relationship");
			xlsx_AttrString(xml, "Id", szId);
			xlsx_AttrString(xml, "Type", type_chartsheet);
			xlsx_AttrString(xml, "Target", buf);
			xlsx_endL(xml);
		}

		if (bFormula) {
			sprintf(szId, "rId%zu", id++);
			xlsx_tagL(xml, "Relationship");
			xlsx_AttrString(xml, "Id", szId);
			xlsx_AttrString(xml, "Type", type_chain);
			xlsx_AttrString(xml, "Target", "calcChain.xml");
			xlsx_endL(xml);
		}

		sprintf(szId, "rId%zu", id++);
		xlsx_tagL(xml, "Relationship");
		xlsx_AttrString(xml, "Id", szId);
		xlsx_AttrString(xml, "Type", type_sharedStr);
		xlsx_AttrString(xml, "Target", "sharedStrings.xml");
		xlsx_endL(xml);

		xlsx_end(xml, "Relationships");
		// zip/xl/_rels/workbook.xml.rels -]
		xlsx_freeWriter(&xml);
	}
	{
		// [- zip/xl/workbook.xml

		char *retpath = registerFile(book->pathManager, "xl/workbook.xml");
		PXML_WRITER xml = NULL;
		xlsx_initWriter(&xml, retpath);
		free(retpath);

		xlsx_tag(xml, "workbook");
		xlsx_AttrString(xml, "xmlns", ns_book);
		xlsx_AttrString(xml, "xmlns:r", ns_book_r);
		xlsx_tagL(xml, "fileVersion");
		xlsx_AttrString(xml, "appName", "xl");
		xlsx_AttrString(xml, "lastEdited", "5");
		/*                  */
		xlsx_AttrString(xml, "lowestEdited", "5");
		xlsx_AttrString(xml, "rupBuild", "9303");
		xlsx_endL(xml);

		xlsx_tagL(xml, "workbookPr");
		xlsx_AttrString(xml, "codeName", "ThisWorkbook");
		xlsx_AttrString(xml, "defaultThemeVersion", "124226");
		xlsx_endL(xml);

		xlsx_tag(xml, "bookViews");

		xlsx_tagL(xml, "workbookView");
		xlsx_AttrString(xml, "xWindow", "270");
		xlsx_AttrString(xml, "yWindow", "630");
		/*                   */
		xlsx_AttrString(xml, "windowWidth", "24615");
		xlsx_AttrString(xml, "windowHeight", "11445");
		/*                   */
		xlsx_AttrInt(xml, "activeTab", book->activeSheetIndex < book->sheetId - 1 ? book->activeSheetIndex : 0);
		xlsx_endL(xml);
		xlsx_end(xml, "bookViews");

		if (!vec_empty(book->worksheets) || !vec_empty(book->chartsheets)) {
			xlsx_tag(xml, "sheets");
			//Sheets ordering
			for (int i = 1; i < book->sheetId; i++) {
				sprintf(szId, "rId%zu", i);
				int Found = 0;
				for (int j = 0; j <= book->worksheets->top; j++) {
					if (i == ((PWORKSHEET)book->worksheets->array[j])->index) {
						Found = 1;
						xlsx_tagL(xml, "sheet");
						xlsx_AttrString(xml, "name", ((PWORKSHEET)book->worksheets->array[j])->title);
						xlsx_AttrInt(xml, "sheetId", i);
						xlsx_AttrString(xml, "r:id", szId);
						xlsx_endL(xml);
						//break;
					}
				}
				if (Found == 1) continue;
				for (int j = 0; j <= book->chartsheets->top; j++) {
					if (i == ((PCHARTSHEET)(book->chartsheets->array[j]))->index) {
						PCHARTSHEET tmp = book->chartsheets->array[j];
						xlsx_tagL(xml, "sheet");
						xlsx_AttrString(xml, "name", tmp->chart->title);
						xlsx_AttrInt(xml, "sheetId", i);
						xlsx_AttrString(xml, "r:id", szId);
						xlsx_endL(xml);
						//break;
					}
				}
			}
			xlsx_end(xml, "sheets");
		}
		xlsx_tagL(xml, "calcPr");
		xlsx_AttrString(xml, "calcId", "124519");
		xlsx_endL(xml);
		xlsx_end(xml, "workbook");
		// zip/xl/workbook.xml -]
		xlsx_freeWriter(&xml);
	}
}
