#include <Chart.h>
#include <PathManager.h>
#include <xmlWriter.h>
#include <xlsxHeaders.h>
#include <vector.h>
#include <string.h>
#include <math.h>


void crt_free(PCHART chart) {
	if (chart == NULL) {
		return;
	}

	if (chart->xmlWriter != NULL) {
		xlsx_freeWriter(&chart->xmlWriter);
	}

	if (chart->diagram != NULL) {
		free(chart->diagram);
		chart->diagram = NULL;
	}

	if (chart->seriesSet != NULL) {
		vec_free(chart->seriesSet);
	}

	if (chart->seriesSetAdd != NULL) {
		vec_free(chart->seriesSetAdd);
	}

	if (chart->xAxis != NULL) {
		free(chart->xAxis);
		chart->xAxis = NULL;
	}

	if (chart->yAxis != NULL) {
		free(chart->yAxis);
		chart->yAxis = NULL;
	}

	if (chart->x2Axis != NULL) {
		free(chart->x2Axis);
		chart->x2Axis = NULL;
	}

	if (chart->y2Axis != NULL) {
		free(chart->y2Axis);
		chart->y2Axis = NULL;
	}

	if (chart->pathManager != NULL) {
		//pm_free(chart->pathManager);
	}

	free(chart);
	chart = NULL;
}

void crt_freeSeries(PSERIES series) {
	if (series == NULL) {
		return;
	}

	if (series->catSheet != NULL) {
		ws_free(series->catSheet);
	}

	if (series->valSheet != NULL) {
		ws_free(series->valSheet);
	}

	free(series->catAxisFrom);
	series->catAxisFrom = NULL;

	free(series->catAxisTo);
	series->catAxisTo = NULL;

	free(series->valAxisFrom);
	series->valAxisFrom = NULL;

	free(series->valAxisTo);
	series->valAxisTo = NULL;

	free(series);
	series = NULL;
}


PCELL_COORD crt_cell_coordInit(int row, int col) {

	PCELL_COORD pcell = NULL;
	pcell = (PCELL_COORD)calloc(1, sizeof(CELL_COORD));
	assert(pcell != NULL);
	pcell->row = row;
	pcell->col = col;

	return pcell;
}

//Put the series initilization in a function because they are in a vector
PSERIES crt_seriesInit(PSERIES series) {
	if (series != NULL) {
		//crt_freeSeries(series);
	}

	series = (PSERIES)calloc(1, sizeof(SERIES));
	series->initialized = 1;
	series->catSheet = NULL;
	series->valSheet = NULL;
	series->LineColor = "";
	series->title = "";
	series->JoinType = joinNone;
	series->LineWidth = 1.0;
	series->DashType = dashSolid;

	series->catAxisFrom = crt_cell_coordInit(0, 1);
	series->catAxisTo = crt_cell_coordInit(0, 1);
	series->valAxisFrom = crt_cell_coordInit(0, 1);
	series->valAxisTo = crt_cell_coordInit(0, 1);

	//Marker initlization
	if (series->Marker != NULL) {
		//crt_free(series->Marker);
	}

	series->Marker = (PMARKER)calloc(1, sizeof(MARKER));
	series->Marker->initialized = 1;

	series->Marker->Size = 7;
	series->Marker->Type = symNone;
	series->Marker->FillColor = "";
	series->Marker->LineColor = "";
	series->Marker->LineWidth = .05;

	return series;
}

//Put the axis initilization in a function because there are 4 in the chart.
PAXIS crt_axisInit(PAXIS axis) {
	if (axis != NULL) {
		//crt_freeAxis(axis);
	}

	axis = (PAXIS)calloc(1, sizeof(AXIS));
	axis->initialized = 1;
	axis->id = 0;
	axis->name = "";
	axis->nameSize = 10;
	axis->pos = POS_LEFT;
	axis->gridLines = GRID_NONE;
	axis->cross = CROSS_AUTO_ZERO;
	axis->sourceLinked = 0;
	axis->minValue = "";
	axis->maxValue = "";
	axis->lblSkipInterval = -1;
	axis->markSkipInterval = -1;
	axis->lblAngle = -1;
	axis->isVal = 1;

	return axis;
}

PCHART crt_init(PCHART chart, size_t index, EChartTypes type, PPATH_MANAGER manager) {
	if (chart != NULL) {
		//crt_free(chart);
	}

	assert(manager != NULL);

	//chart initilization
	chart = (PCHART)calloc(1, sizeof(CHART));

	assert(chart != NULL);

	chart->initialized = 1;
	chart->pathManager = manager;
	chart->index = index;

	//XML Writer Initlization
	char buf[2048];
	snprintf(buf, 2048, "xl/charts/chart%d.xml", chart->index);
	char *retpath = registerFile(chart->pathManager, buf);

	xlsx_initWriter(&chart->xmlWriter, retpath);
	free(retpath);

	assert(chart->xmlWriter != NULL);

	//Axis initlization before setting values
	chart->xAxis = crt_axisInit(chart->xAxis);
	assert(chart->xAxis != NULL);
	chart->yAxis = crt_axisInit(chart->yAxis);
	assert(chart->yAxis != NULL);
	chart->x2Axis = crt_axisInit(chart->x2Axis);
	assert(chart->x2Axis != NULL);
	chart->y2Axis = crt_axisInit(chart->y2Axis);
	assert(chart->y2Axis != NULL);

	//Back to chart initlization
	chart->title = "Chart 1";
	chart->xAxis->id = 100;
	chart->yAxis->id = 101;
	chart->x2Axis->id = 102;
	chart->y2Axis->id = 103;

	chart->xAxis->pos = POS_BOTTOM;
	chart->xAxis->sourceLinked = 0;

	chart->yAxis->pos = POS_LEFT;
	chart->yAxis->sourceLinked = 1;

	chart->x2Axis->pos = POS_TOP;
	chart->x2Axis->sourceLinked = 0;
	chart->x2Axis->cross = CROSS_MAX;

	chart->y2Axis->pos = POS_RIGHT;
	chart->y2Axis->sourceLinked = 1;
	chart->y2Axis->cross = CROSS_MAX;

	//Diagram initilization
	chart->diagram = (PDIAGRAM)calloc(1, sizeof(DIAGRAM));
	assert(chart->diagram != NULL);
	chart->diagram->initialized = 1;
	chart->diagram->name = "";
	chart->diagram->nameSize = 18;
	chart->diagram->legend_pos = POS_RIGHT;
	chart->diagram->tableData = TBL_DATA_NONE;
	chart->diagram->typeMain = CHART_LINEAR;
	chart->diagram->typeAdditional = CHART_NONE;
	chart->diagram->barDir = BAR_DIR_VERTICAL;
	chart->diagram->barGroup = BAR_GROUP_CLUSTERED;
	chart->diagram->scatterStyle = SCATTER_POINT;
	chart->diagram->typeMain = type;

	//Series initilization. This is a vector of series
	assert(chart->seriesSet == NULL);
	chart->seriesSet = vec_init();
	assert(chart->seriesSet != NULL);
	assert(chart->seriesSetAdd == NULL);
	chart->seriesSetAdd = vec_init();
	assert(chart->seriesSetAdd != NULL);

	return chart;
}

void GetCharForPos(char retValBuf[], EPosition Pos, char* DefaultChar) {
	char* retval = "";

	switch (Pos)
	{
	case POS_LEFT:         retval = "l";
	case POS_RIGHT:        retval = "r";
	case POS_TOP:          retval = "t";
	case POS_BOTTOM:       retval = "t";
	case POS_LEFT_ASIDE:   retval = DefaultChar;
	case POS_RIGHT_ASIDE:  retval = DefaultChar;
	case POS_TOP_ASIDE:    retval = DefaultChar;
	case POS_BOTTOM_ASIDE: retval = DefaultChar;
	case POS_NONE:         retval = DefaultChar;
	}
	snprintf(retValBuf, 2, "%s", retval);
}

void crt_addXAxis(PCHART chart, PAXIS axis, uint32_t crossAxisId) {
	PXML_WRITER xml = chart->xmlWriter;

	xlsx_tag(xml, axis->isVal == 1 ? "c:valAx" : "c:catAx");
	xlsx_tagL(xml, "c:axId");
	xlsx_AttrInt(xml, "val", axis->id);
	xlsx_endL(xml);

	xlsx_tag(xml, "c:scaling");
	xlsx_tagL(xml, "c:orientation");
	xlsx_AttrString(xml, "val", "minMax");
	xlsx_endL(xml);

	if (axis->minValue[0] != 0) {
		xlsx_tagL(xml, "c:min");
		xlsx_AttrString(xml, "val", axis->minValue);
		xlsx_endL(xml);

	}
	if (axis->maxValue[0] != 0) {
		xlsx_tagL(xml, "c:max");
		xlsx_AttrString(xml, "val", axis->maxValue);
		xlsx_endL(xml);
	}

	xlsx_end(xml, "c:scaling");

	if (axis->pos == POS_NONE) {
		xlsx_tagL(xml, "c:delete");
		xlsx_AttrString(xml, "val", "1");
		xlsx_endL(xml);
	}
	else {
		xlsx_tagL(xml, "c:delete");
		xlsx_AttrString(xml, "val", "0");
		xlsx_endL(xml);
	}

	char axPosChar[2] = "";
	GetCharForPos(axPosChar, axis->pos, "b");

	xlsx_tagL(xml, "c:axPos");
	xlsx_AttrString(xml, "val", axPosChar);
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:minorTickMark");
	xlsx_AttrString(xml, "val", "none");
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:majorTickMark");
	xlsx_AttrString(xml, "val", "none");
	xlsx_endL(xml);

	if (axis->gridLines == GRID_MAJOR) {
		xlsx_tagL(xml, "c:majorGridlines");
		xlsx_endL(xml);
	}
	else if (axis->gridLines == GRID_MINOR) {
		xlsx_tagL(xml, "c:minorGridlines");
		xlsx_endL(xml);
	}
	else if (axis->gridLines == GRID_MAJOR_N_MINOR) {
		xlsx_tagL(xml, "c:majorGridlines");
		xlsx_endL(xml);
		xlsx_tagL(xml, "c:minorGridlines");
		xlsx_endL(xml);
	}

	if (axis->name[0] != 0)
	{
		crt_addTitle(chart, axis->name, axis->nameSize, 0);
	}

	xlsx_tagL(xml, "c:tickLblPos");
	xlsx_AttrString(xml, "val", "nextTo");
	xlsx_endL(xml);

	if (axis->lblAngle != -1)
	{
		xlsx_tag(xml, "c:txPr");
		xlsx_tagL(xml, "a:bodyPr");
		xlsx_AttrInt(xml, "rot", axis->lblAngle * 60000);
		xlsx_endL(xml);
		xlsx_tagL(xml, "a:lstStyle");
		xlsx_endL(xml);

		xlsx_tag(xml, "a:p");
		xlsx_tag(xml, "a:pPr");
		xlsx_tagL(xml, "a:defRPr");
		xlsx_endL(xml);
		xlsx_end(xml, "a:pPr");

		xlsx_tagL(xml, "a:endParaRPr");
		xlsx_AttrString(xml, "lang", "en-US");
		xlsx_endL(xml);
		xlsx_end(xml, "a:p");
		xlsx_end(xml, "c:txPr");
	}
	if (crossAxisId != 0)
	{
		xlsx_tagL(xml, "c:crossAx");
		xlsx_AttrInt(xml, "val", crossAxisId);
		xlsx_endL(xml);
		xlsx_tagL(xml, "c:crosses");

		if (axis->cross == CROSS_AUTO_ZERO) {
			xlsx_AttrString(xml, "val", "autoZero");
			xlsx_endL(xml);
		}
		else if (axis->cross == CROSS_MIN) {
			xlsx_AttrString(xml, "val", "min");
			xlsx_endL(xml);
		}
		else if (axis->cross == CROSS_MAX) {
			xlsx_AttrString(xml, "val", "max");
			xlsx_endL(xml);
		}
		else xlsx_endL(xml);
	}

	xlsx_tagL(xml, "c:auto");
	xlsx_AttrString(xml, "val", "1");
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:lblAlgn");
	xlsx_AttrString(xml, "val", "ctr");
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:lblOffset");
	xlsx_AttrString(xml, "val", "100");
	xlsx_endL(xml);

	if (axis->lblSkipInterval != -1) {
		xlsx_tagL(xml, "c:tickLblSkip");
		xlsx_AttrInt(xml, "val", axis->lblSkipInterval);
		xlsx_endL(xml);
	}

	if (axis->markSkipInterval != -1) {
		xlsx_tagL(xml, "c:tickMarkSkip");
		xlsx_AttrInt(xml, "val", axis->markSkipInterval);
		xlsx_endL(xml);
	}

	xlsx_tagL(xml, "c:noMultiLvlLbl");
	xlsx_AttrString(xml, "val", "0");
	xlsx_endL(xml);

	xlsx_end(xml, axis->isVal == 1 ? "c:valAx" : "c:catAx");
}

void crt_addYAxis(PCHART chart, PAXIS axis, uint32_t crossAxisId) {
	PXML_WRITER xml = chart->xmlWriter;

	xlsx_tag(xml, axis->isVal ? "c:valAx" : "c:catAx");

	xlsx_tagL(xml, "c:axId");
	xlsx_AttrInt(xml, "val", axis->id);
	xlsx_endL(xml);

	xlsx_tag(xml, "c:scaling");
	xlsx_tagL(xml, "c:orientation");
	xlsx_AttrString(xml, "val", "minMax");
	xlsx_endL(xml);

	if (axis->minValue[0] != 0) {
		xlsx_tagL(xml, "c:min");
		xlsx_AttrString(xml, "val", axis->minValue);
		xlsx_endL(xml);
	}
	if (axis->maxValue[0] != 0) {
		xlsx_tagL(xml, "c:max");
		xlsx_AttrString(xml, "val", axis->maxValue);
		xlsx_endL(xml);
	}

	xlsx_end(xml, "c:scaling");

	if (axis->pos == POS_NONE) {
		xlsx_tagL(xml, "c:delete");
		xlsx_AttrString(xml, "val", "1");
		xlsx_endL(xml);
	}
	else {
		xlsx_tagL(xml, "c:delete");
		xlsx_AttrString(xml, "val", "0");
		xlsx_endL(xml);
	}

	char axPosChar[2] = "";
	GetCharForPos(axPosChar, axis->pos, "l");

	xlsx_tagL(xml, "c:axPos");
	xlsx_AttrString(xml, "val", axPosChar);
	xlsx_endL(xml);

	if (axis->gridLines == GRID_MAJOR) {
		xlsx_tagL(xml, "c:majorGridlines");
		xlsx_endL(xml);
	}
	else if (axis->gridLines == GRID_MINOR) {
		xlsx_tagL(xml, "c:minorGridlines");
		xlsx_endL(xml);
	}
	else if (axis->gridLines == GRID_MAJOR_N_MINOR) {
		xlsx_tagL(xml, "c:majorGridlines");
		xlsx_endL(xml);
		xlsx_tagL(xml, "c:minorGridlines");
		xlsx_endL(xml);
	}

	if (axis->name[0] != 0)
	{
		crt_addTitle(chart, axis->name, axis->nameSize, 1);
	}

	if (axis->sourceLinked) {
		xlsx_tagL(xml, "c:numFmt");
		xlsx_AttrString(xml, "formatCode", "General");
		xlsx_AttrString(xml, "sourceLinked", "1");
		xlsx_endL(xml);
	}

	xlsx_tagL(xml, "c:majorTickMark");
	xlsx_AttrString(xml, "val", "none");
	xlsx_endL(xml);
	xlsx_tagL(xml, "c:minorTickMark");
	xlsx_AttrString(xml, "val", "none");
	xlsx_endL(xml);
	xlsx_tagL(xml, "c:tickLblPos");
	xlsx_AttrString(xml, "val", "nextTo");
	xlsx_endL(xml);

	if (crossAxisId != 0) {
		xlsx_tagL(xml, "c:crossAx");
		xlsx_AttrInt(xml, "val", crossAxisId);
		xlsx_endL(xml);
		xlsx_tagL(xml, "c:crosses");

		if (axis->cross == CROSS_AUTO_ZERO) {
			xlsx_AttrString(xml, "val", "autoZero");
			xlsx_endL(xml);
		}
		else if (axis->cross == CROSS_MIN) {
			xlsx_AttrString(xml, "val", "min");
			xlsx_endL(xml);
		}
		else if (axis->cross == CROSS_MAX) {
			xlsx_AttrString(xml, "val", "max");
			xlsx_endL(xml);
		}
		else  xlsx_endL(xml);

		xlsx_tagL(xml, "c:crossBetween");
		xlsx_AttrString(xml, "val", "between");
		xlsx_endL(xml);
	}

	xlsx_end(xml, axis->isVal ? "c:valAx" : "c:catAx");
}

int crt_addSeries(PCHART chart, PSERIES series, int mainChart) {

	if ((series->valSheet == NULL) || ((series->valAxisTo->row == 0) && (series->valAxisTo->col == 0)))
	{
		return -1;
	}

	if (mainChart)
	{
		if ((chart->diagram->typeMain == CHART_SCATTER) && ((series->catSheet == NULL) ||
			((series->catAxisTo->row == 0) && (series->catAxisTo->col == 0))))
		{
			return -2;
		}

		vec_pushback(chart->seriesSet, series);
	}
	else
	{
		if ((chart->diagram->typeAdditional == CHART_SCATTER) && ((series->catSheet == NULL) ||
			((series->catAxisTo->row == 0) && (series->catAxisTo->col == 0))))
		{
			return -3;
		}

		vec_pushback(chart->seriesSetAdd, series);
	}

	return 1;
}

void crt_addLegend(PCHART chart) {
	PXML_WRITER xml = chart->xmlWriter;

	int overlay = 1;
	char pos = '\0';
	switch (chart->diagram->legend_pos)
	{
	case POS_NONE:  return;

	case POS_LEFT_ASIDE:   overlay = 0; pos = 'l'; break;
	case POS_LEFT:   pos = 'l'; break;

	case POS_RIGHT_ASIDE:  overlay = 0; pos = 'r'; break;
	case POS_RIGHT:  pos = 'r'; break;

	case POS_TOP_ASIDE:    overlay = 0; pos = 't'; break;
	case POS_TOP:    pos = 't'; break;

	case POS_BOTTOM_ASIDE: overlay = 0; pos = 'b'; break;
	case POS_BOTTOM: pos = 'b'; break;
	}

	xlsx_tag(xml, "c:legend");

	xlsx_tagL(xml, "c:legendPos");

	char tpos[2];
	snprintf(tpos, 2, "%c\0", pos);

	xlsx_AttrString(xml, "val", tpos);
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:layout");
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:overlay");
	xlsx_AttrInt(xml, "val", overlay);
	xlsx_endL(xml);

	xlsx_end(xml, "c:legend");
}

void crt_addTableData(PCHART chart) {
	PXML_WRITER xml = chart->xmlWriter;

	int showKeys = 1;

	switch (chart->diagram->tableData)
	{
	case TBL_DATA_NONE:   return;
	case TBL_DATA:        break;
	case TBL_DATA_N_KEYS: showKeys = 0; break;
		//                default:                break;
	}

	xlsx_tag(xml, "c:dTable");

	xlsx_tagL(xml, "c:showHorzBorder");
	xlsx_AttrString(xml, "val", "1");
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:showVertBorder");
	xlsx_AttrString(xml, "val", "1");
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:showOutline");
	xlsx_AttrString(xml, "val", "1");
	xlsx_endL(xml);

	if (showKeys == 1) {
		xlsx_tagL(xml, "c:showKeys");
		xlsx_AttrString(xml, "val", "1");
		xlsx_endL(xml);
	}

	xlsx_end(xml, "c:dTable");
}

void crt_addTitle(PCHART chart, const char * name, int size, int vertPos) {
	PXML_WRITER xml = chart->xmlWriter;

	xlsx_tag(xml, "c:title");
	xlsx_tag(xml, "c:tx");
	xlsx_tag(xml, "c:rich");
	xlsx_tag(xml, "a:bodyPr");

	if (vertPos == 1) {
		xlsx_AttrString(xml, "rot", "-5400000");
		xlsx_AttrString(xml, "vert", "horz");
	}

	xlsx_end(xml, "a:bodyPr");
	xlsx_tagL(xml, "a:lstStyle");
	xlsx_endL(xml);

	xlsx_tag(xml, "a:p");
	xlsx_tag(xml, "a:pPr");
	xlsx_tagL(xml, "a:defRPr");
	xlsx_AttrString(xml, "lang", "en-US");
	xlsx_AttrInt(xml, "sz", size * 100);
	xlsx_endL(xml);
	xlsx_end(xml, "a:pPr");

	xlsx_tag(xml, "a:r");

	xlsx_tagL(xml, "a:rPr");
	xlsx_AttrString(xml, "lang", "en-US");
	xlsx_AttrInt(xml, "sz", size * 100);
	xlsx_endL(xml);

	xlsx_tagOnlyString(xml, "a:t", name);
	xlsx_end(xml, "a:r");

	xlsx_tagL(xml, "a:endParaRPr");
	xlsx_AttrString(xml, "lang", "en-US");
	xlsx_endL(xml);

	xlsx_end(xml, "a:p");
	xlsx_end(xml, "c:rich");
	xlsx_end(xml, "c:tx");

	xlsx_tagL(xml, "c:layout");
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:overlay");
	xlsx_AttrString(xml, "val", "0");
	xlsx_endL(xml);

	xlsx_end(xml, "c:title");
}

void crt_addLineChart(PCHART chart, PAXIS axis, uint32_t yAxisId, PVECTOR seriesVec, size_t firstSeriesId)
{
	PXML_WRITER xml = chart->xmlWriter;

	PCELL_COORD cellCoordFrom = (PCELL_COORD)calloc(1, sizeof(CELL_COORD));
	PCELL_COORD cellCoordTo = (PCELL_COORD)calloc(1, sizeof(CELL_COORD));

	xlsx_tag(xml, "c:lineChart");

	xlsx_tagL(xml, "c:grouping");
	xlsx_AttrString(xml, "val", "standard");
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:varyColors");
	xlsx_AttrString(xml, "val", "0");
	xlsx_endL(xml);

	PSERIES tmpSer = (PSERIES)calloc(1, sizeof(SERIES));

	for (int i = 0; i <= seriesVec->top; i++) {
		tmpSer = (PSERIES)seriesVec->array[i];

		xlsx_tag(xml, "c:ser");

		xlsx_tagL(xml, "c:idx");
		xlsx_AttrInt(xml, "val", firstSeriesId);
		xlsx_endL(xml);

		xlsx_tagL(xml, "c:order");
		xlsx_AttrInt(xml, "val", firstSeriesId);
		xlsx_endL(xml);

		const char * markerID = "none";

		switch (tmpSer->Marker->Type) {
		case symDiamond:  markerID = "diamond";
			break;
		case symCircle:   markerID = "circle";
			break;
		case symSquare:   markerID = "square";
			break;
		case symTriangle: markerID = "triangle";
			break;
		default:;
		}

		xlsx_tag(xml, "c:marker");
		xlsx_tagL(xml, "c:symbol");
		xlsx_AttrString(xml, "val", markerID);
		xlsx_endL(xml);
		xlsx_tagL(xml, "c:size");
		xlsx_AttrInt(xml, "val", tmpSer->Marker->Size);
		xlsx_endL(xml);

		if (strlen(tmpSer->Marker->FillColor) == 6 && strlen(tmpSer->Marker->LineColor) == 6) {
			xlsx_tag(xml, "c:spPr");
			xlsx_tag(xml, "a:solidFill");
			xlsx_tagL(xml, "a:srgbClr");
			xlsx_AttrString(xml, "val", tmpSer->Marker->FillColor);
			xlsx_endL(xml);
			xlsx_end(xml, "a:solidFill");
			xlsx_tag(xml, "a:ln");
			xlsx_AttrDouble(xml, "w", floor(tmpSer->Marker->LineWidth * 12700));
			xlsx_tag(xml, "a:solidFill");
			xlsx_tagL(xml, "a:srgbClr");
			xlsx_AttrString(xml, "val", tmpSer->Marker->LineColor);
			xlsx_endL(xml);
			xlsx_end(xml, "a:solidFill");
			xlsx_end(xml, "a:ln");
			xlsx_end(xml, "c:spPr");
		};
		xlsx_end(xml, "c:marker");


		if (tmpSer->title[0] != 0) {
			xlsx_tag(xml, "c:tx");
			xlsx_tag(xml, "c:strRef");
			xlsx_tag(xml, "c:strCache");
			xlsx_tagL(xml, "c:ptCount");
			xlsx_AttrString(xml, "val", "1");
			xlsx_endL(xml);
			xlsx_tag(xml, "c:pt");
			xlsx_AttrString(xml, "idx", "0");
			xlsx_tagOnlyString(xml, "c:v", tmpSer->title);
			xlsx_end(xml, "c:pt");
			xlsx_end(xml, "c:strCache");
			xlsx_end(xml, "c:strRef");
			xlsx_end(xml, "c:tx");
		}

		if (tmpSer->JoinType == joinNone) {
			xlsx_tag(xml, "c:spPr");
			xlsx_tag(xml, "a:ln");
			xlsx_AttrString(xml, "w", "28000");
			xlsx_tagL(xml, "a:noFill");
			xlsx_endL(xml);
			xlsx_end(xml, "a:ln");
			xlsx_end(xml, "c:spPr");
		}
		else {
			const char * dashID = "solid";
			switch (tmpSer->DashType) {
				case
				dashDot:       dashID = "sysDot";
					break;
					case
					dashShortDash: dashID = "sysDash";
						break;
						case
						dashDash:      dashID = "dash";
							break;
						default:;
			}
			xlsx_tag(xml, "c:spPr");
			xlsx_tag(xml, "a:ln");
			xlsx_AttrDouble(xml, "w", floor(tmpSer->LineWidth * 12700));

			if (strlen(tmpSer->LineColor) == 6) {
				xlsx_tag(xml, "a:solidFill");
				xlsx_tagL(xml, "a:srgbClr");
				xlsx_AttrString(xml, "val", tmpSer->LineColor);
				xlsx_endL(xml);
				xlsx_end(xml, "a:solidFill");
			}

			xlsx_tagL(xml, "a:prstDash");
			xlsx_AttrString(xml, "val", dashID);
			xlsx_endL(xml);
			xlsx_end(xml, "a:ln");
			xlsx_end(xml, "c:spPr");
		}

		if ((tmpSer->catSheet != NULL) && ((tmpSer->catAxisTo->row != 0) || (tmpSer->catAxisTo->col != 0))) {
			axis->sourceLinked = 1;
			char cfRange[256] = "";

			cellCoordFrom->row = tmpSer->catAxisFrom->row;
			cellCoordFrom->col = tmpSer->catAxisFrom->col;

			cellCoordTo->row = tmpSer->catAxisTo->row;
			cellCoordTo->col = tmpSer->catAxisTo->col;

			crt_cellRangeString(cfRange, tmpSer->catSheet->title, cellCoordFrom, cellCoordTo);
			xlsx_tag(xml, "c:cat");
			xlsx_tag(xml, "c:numRef");
			xlsx_tagOnlyString(xml, "c:f", cfRange);
			xlsx_end(xml, "c:numRef");
			xlsx_end(xml, "c:cat");

		}


		//WIP
		char cfRange[256] = "";

		cellCoordFrom->row = tmpSer->valAxisFrom->row;
		cellCoordFrom->col = tmpSer->valAxisFrom->col;

		cellCoordTo->row = tmpSer->valAxisTo->row;
		cellCoordTo->col = tmpSer->valAxisTo->col;

		crt_cellRangeString(cfRange, tmpSer->valSheet->title, cellCoordFrom, cellCoordTo);

		xlsx_tag(xml, "c:val");
		xlsx_tag(xml, "c:numRef");
		xlsx_tagOnlyString(xml, "c:f", cfRange);
		xlsx_end(xml, "c:numRef");
		xlsx_end(xml, "c:val");
		xlsx_tagL(xml, "c:smooth");
		xlsx_AttrString(xml, "val", tmpSer->JoinType == joinSmooth ? "1" : "0");
		xlsx_endL(xml);
		xlsx_end(xml, "c:ser");

		firstSeriesId++;
		//free(cellCoordFrom);
		//free(cellCoordTo);
	}

	xlsx_tagL(xml, "c:marker");
	xlsx_AttrString(xml, "val", "1");
	xlsx_endL(xml);
	xlsx_tagL(xml, "c:smooth");
	xlsx_AttrString(xml, "val", "0");
	xlsx_endL(xml);
	xlsx_tagL(xml, "c:axId");
	xlsx_AttrInt(xml, "val", axis->id);
	xlsx_endL(xml);
	xlsx_tagL(xml, "c:axId");
	xlsx_AttrInt(xml, "val", yAxisId);
	xlsx_endL(xml);

	xlsx_end(xml, "c:lineChart");
}

void crt_addBarChart(PCHART chart, PAXIS xAxis, uint32_t yAxisId, PVECTOR seriesVec,
	size_t firstSeriesId, EBarDirection barDir, EBarGrouping barGroup)
{
	PXML_WRITER xml = chart->xmlWriter;

	xlsx_tag(xml, "c:barChart");
	if (barDir == BAR_DIR_VERTICAL) {
		xlsx_tagL(xml, "c:barDir");
		xlsx_AttrString(xml, "val", "col");
		xlsx_endL(xml);
	}
	else if (barDir == BAR_DIR_HORIZONTAL) {
		xlsx_tagL(xml, "c:barDir");
		xlsx_AttrString(xml, "val", "bar");
		xlsx_endL(xml);
	}

	if (barGroup == BAR_GROUP_CLUSTERED) {
		xlsx_tagL(xml, "c:grouping");
		xlsx_AttrString(xml, "val", "clustered");
		xlsx_endL(xml);
	}
	else if (barGroup == BAR_GROUP_STACKED) {
		xlsx_tagL(xml, "c:grouping");
		xlsx_AttrString(xml, "val", "stacked");
		xlsx_endL(xml);
	}
	else if (barGroup == BAR_GROUP_PERCENT_STACKED) {
		xlsx_tagL(xml, "c:grouping");
		xlsx_AttrString(xml, "val", "percentStacked");
		xlsx_endL(xml);
	}

	PSERIES tmpSer = (PSERIES)calloc(1, sizeof(SERIES));

	for (int i = 0; i <= seriesVec->top; i++) {
		tmpSer = (PSERIES)seriesVec->array[i];
		xlsx_tag(xml, "c:ser");
		xlsx_tagL(xml, "c:idx");
		xlsx_AttrInt(xml, "val", firstSeriesId);
		xlsx_endL(xml);

		xlsx_tagL(xml, "c:order");
		xlsx_AttrInt(xml, "val", firstSeriesId);
		xlsx_endL(xml);

		if (tmpSer->title[0] != 0) {
			xlsx_tag(xml, "c:tx");
			xlsx_tagOnlyString(xml, "c:v", tmpSer->title);
			xlsx_end(xml, "c:tx");
		}
		if ((tmpSer->catSheet != NULL) && ((tmpSer->catAxisTo->row != 0) || (tmpSer->catAxisTo->col != 0))) {
			xAxis->sourceLinked = 1;

			char cfRange[256] = "";
			PCELL_COORD catFrom, catTo;
			catFrom = (PCELL_COORD)calloc(1, sizeof(CELL_COORD));
			catTo = (PCELL_COORD)calloc(1, sizeof(CELL_COORD));

			catFrom->row = tmpSer->catAxisFrom->row;
			catFrom->col = tmpSer->catAxisFrom->col;

			catTo->row = tmpSer->catAxisTo->row;
			catTo->col = tmpSer->catAxisTo->col;

			crt_cellRangeString(cfRange, tmpSer->catSheet->title, catFrom, catTo);

			xlsx_tag(xml, "c:cat");
			xlsx_tag(xml, "c:numRef");
			xlsx_tagOnlyString(xml, "c:f", cfRange);
			xlsx_end(xml, "c:numRef");
			xlsx_end(xml, "c:cat");

			free(catFrom);
			free(catTo);
		}

		char cfRange[256] = "";
		PCELL_COORD valFrom, valTo;
		valFrom = (PCELL_COORD)calloc(1, sizeof(CELL_COORD));
		valTo = (PCELL_COORD)calloc(1, sizeof(CELL_COORD));

		valFrom->row = tmpSer->valAxisFrom->row;
		valFrom->col = tmpSer->valAxisFrom->col;

		valTo->row = tmpSer->valAxisTo->row;
		valTo->col = tmpSer->valAxisTo->col;

		crt_cellRangeString(cfRange, tmpSer->valSheet->title, valFrom, valTo);

		xlsx_tag(xml, "c:val");
		xlsx_tag(xml, "c:numRef");
		xlsx_tagOnlyString(xml, "c:f", cfRange);
		xlsx_end(xml, "c:numRef");
		xlsx_end(xml, "c:val");

		xlsx_tagL(xml, "c:smooth");
		xlsx_AttrString(xml, "val", tmpSer->JoinType == joinSmooth ? "1" : "0");
		xlsx_endL(xml);

		xlsx_end(xml, "c:ser");

		firstSeriesId++;

		free(valFrom);
		free(valTo);
	}

	xlsx_tagL(xml, "c:marker");
	xlsx_AttrString(xml, "val", "1");
	xlsx_endL(xml);
	xlsx_tagL(xml, "c:smooth");
	xlsx_AttrString(xml, "val", "0");
	xlsx_endL(xml);
	xlsx_tagL(xml, "c:axId");
	xlsx_AttrInt(xml, "val", (int)xAxis->id);
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:axId");
	xlsx_AttrInt(xml, "val", yAxisId);
	xlsx_endL(xml);

	xlsx_end(xml, "c:barChart");
}

void crt_addScatterChart(PCHART chart, uint32_t xAxisId, uint32_t yAxisId, PVECTOR seriesVec, size_t firstSeriesId, EScatterStyle style) {
	PXML_WRITER xml = chart->xmlWriter;

	xlsx_tag(xml, "c:scatterChart");

	if (style == SCATTER_FILL) {
		xlsx_tagL(xml, "c:scatterStyle");
		xlsx_AttrString(xml, "val", "smoothMarker");
		xlsx_endL(xml);
	}
	else if (style == SCATTER_POINT) {
		xlsx_tagL(xml, "c:scatterStyle");
		xlsx_AttrString(xml, "val", "lineMarker");
		xlsx_endL(xml);
	}
	xlsx_tagL(xml, "c:varyColors");
	xlsx_AttrString(xml, "val", "0");
	xlsx_endL(xml);

	PSERIES tmpSer = (PSERIES)calloc(1, sizeof(SERIES));

	for (int i = 0; i <= seriesVec->top; i++) {
		tmpSer = (PSERIES)seriesVec->array[i];
		xlsx_tag(xml, "c:ser");
		xlsx_tagL(xml, "c:idx");
		xlsx_AttrInt(xml, "val", firstSeriesId);
		xlsx_endL(xml);
		xlsx_tagL(xml, "c:order");
		xlsx_AttrInt(xml, "val", firstSeriesId);
		xlsx_endL(xml);

		const char * markerID = "none";
		if (style == SCATTER_POINT)
			switch (tmpSer->Marker->Type) {
				case
				symDiamond:  markerID = "diamond";
					break;
					case
					symCircle:   markerID = "circle";
						break;
						case
						symSquare:   markerID = "square";
							break;
							case
							symTriangle: markerID = "triangle";
								break;
							default:;
			}
		xlsx_tag(xml, "c:marker");
		xlsx_tagL(xml, "c:symbol");
		xlsx_AttrString(xml, "val", markerID);
		xlsx_endL(xml);
		xlsx_tagL(xml, "c:size");
		xlsx_AttrInt(xml, "val", tmpSer->Marker->Size);
		xlsx_endL(xml);

		if (strlen(tmpSer->Marker->FillColor) == 6 && strlen(tmpSer->Marker->LineColor) == 6) { // check formal RGB record format
			xlsx_tag(xml, "c:spPr");
			xlsx_tag(xml, "a:solidFill");
			xlsx_tagL(xml, "a:srgbClr");
			xlsx_AttrString(xml, "val", tmpSer->Marker->FillColor);
			xlsx_endL(xml);
			xlsx_end(xml, "a:solidFill"); // marker fill
			xlsx_tag(xml, "a:ln");
			xlsx_AttrDouble(xml, "w", (int)(tmpSer->Marker->LineWidth * 12700));
			xlsx_tag(xml, "a:solidFill");
			xlsx_tagL(xml, "a:srgbClr");
			xlsx_AttrString(xml, "val", tmpSer->Marker->LineColor);
			xlsx_endL(xml);
			xlsx_end(xml, "a:solidFill");
			xlsx_end(xml, "a:ln"); // marker line
			xlsx_end(xml, "c:spPr");
		};
		xlsx_end(xml, "c:marker");

		if (tmpSer->title[0] != 0) {
			xlsx_tag(xml, "c:tx");
			xlsx_tagOnlyString(xml, "c:v", tmpSer->title);
			xlsx_end(xml, "c:tx");
		}

		if (tmpSer->JoinType == joinNone) {
			xlsx_tag(xml, "c:spPr");
			xlsx_tag(xml, "a:ln");
			xlsx_AttrString(xml, "w", "28000");
			xlsx_tagL(xml, "a:noFill");
			xlsx_endL(xml);
			xlsx_end(xml, "a:ln");
			xlsx_end(xml, "c:spPr");
		}
		else {
			const char * dashID = "solid";
			switch (tmpSer->DashType) {
				case
				dashDot:       dashID = "sysDot";
					break;
					case
					dashShortDash: dashID = "sysDash";
						break;
						case
						dashDash:      dashID = "dash";
							break;
						default:;
			}
			xlsx_tag(xml, "c:spPr");
			xlsx_tag(xml, "a:ln");
			xlsx_AttrDouble(xml, "w", floor(tmpSer->LineWidth * 12700));

			if (strlen(tmpSer->LineColor) == 6) {
				xlsx_tag(xml, "a:solidFill");
				xlsx_tagL(xml, "a:srgbClr");
				xlsx_AttrString(xml, "val", tmpSer->LineColor);
				xlsx_endL(xml);
				xlsx_end(xml, "a:solidFill");
			}

			xlsx_tagL(xml, "a:prstDash");
			xlsx_AttrString(xml, "val", dashID);
			xlsx_endL(xml);
			xlsx_end(xml, "a:ln");
			xlsx_end(xml, "c:spPr");

		}

		char cfRange[256] = "";
		PCELL_COORD catFrom = (PCELL_COORD)calloc(1, sizeof(CELL_COORD));
		PCELL_COORD catTo = (PCELL_COORD)calloc(1, sizeof(CELL_COORD));

		catFrom->row = tmpSer->catAxisFrom->row;
		catFrom->col = tmpSer->catAxisFrom->col;

		catTo->row = tmpSer->catAxisTo->row;
		catTo->col = tmpSer->catAxisTo->col;

		crt_cellRangeString(cfRange, tmpSer->catSheet->title, catFrom, catTo);

		xlsx_tag(xml, "c:xVal");
		xlsx_tag(xml, "c:numRef");
		xlsx_tagOnlyString(xml, "c:f", cfRange);
		xlsx_end(xml, "c:numRef");
		xlsx_end(xml, "c:xVal");

		memset(&cfRange[0], 0, sizeof(cfRange));
		PCELL_COORD valFrom = (PCELL_COORD)calloc(1, sizeof(CELL_COORD));
		PCELL_COORD valTo = (PCELL_COORD)calloc(1, sizeof(CELL_COORD));

		valFrom->row = tmpSer->valAxisFrom->row;
		valFrom->col = tmpSer->valAxisFrom->col;

		valTo->row = tmpSer->valAxisTo->row;
		valTo->col = tmpSer->valAxisTo->col;

		crt_cellRangeString(cfRange, tmpSer->valSheet->title, catFrom, catTo);

		xlsx_tag(xml, "c:yVal");
		xlsx_tag(xml, "c:numRef");
		xlsx_tagOnlyString(xml, "c:f", cfRange);
		xlsx_end(xml, "c:numRef");
		xlsx_end(xml, "c:yVal");
		xlsx_tagL(xml, "c:smooth");
		xlsx_AttrString(xml, "val", tmpSer->JoinType == joinSmooth ? "1" : "0");
		xlsx_endL(xml);

		xlsx_end(xml, "c:ser");

		firstSeriesId++;
	}
	xlsx_tagL(xml, "c:marker");
	xlsx_AttrString(xml, "val", "1");
	xlsx_endL(xml);
	xlsx_tagL(xml, "c:smooth");
	xlsx_AttrString(xml, "val", "0");
	xlsx_endL(xml);
	xlsx_tagL(xml, "c:axId");
	xlsx_AttrInt(xml, "val", xAxisId);
	xlsx_endL(xml);
	xlsx_tagL(xml, "c:axId");
	xlsx_AttrInt(xml, "val", yAxisId);
	xlsx_endL(xml);

	xlsx_end(xml, "c:scatterChart");
}

int crt_save(PCHART chart) {

	PXML_WRITER xml = chart->xmlWriter;
	if (vec_empty(chart->seriesSet)) return -1;

	xlsx_tag(xml, "c:chartSpace");
	xlsx_AttrString(xml, "xmlns:c", ns_c);
	xlsx_AttrString(xml, "xmlns:a", ns_a);
	xlsx_AttrString(xml, "xmlns:r", ns_relationships_chart);

	xlsx_tagL(xml, "c:date1904");
	xlsx_AttrString(xml, "val", "0");
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:lang");
	xlsx_AttrString(xml, "val", "en-US");
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:roundedCorners");
	xlsx_AttrString(xml, "val", "0");
	xlsx_endL(xml);

	xlsx_tag(xml, "mc:AlternateContent");
	xlsx_AttrString(xml, "xmlns:mc", ns_markup_compatibility);

	xlsx_tag(xml, "mc:Choice");
	xlsx_AttrString(xml, "Requires", "c14");
	xlsx_AttrString(xml, "xmlns:c14", ns_c14);

	xlsx_tagL(xml, "c14:style");
	xlsx_AttrString(xml, "val", "102");
	xlsx_endL(xml);
	xlsx_end(xml, "mc:Choice");

	xlsx_tag(xml, "mc:Fallback");
	xlsx_tagL(xml, "c:style");
	xlsx_AttrString(xml, "val", "2");
	xlsx_endL(xml);
	xlsx_end(xml, "mc:Fallback");

	xlsx_end(xml, "mc:AlternateContent");

	xlsx_tag(xml, "c:chart");

	if (chart->diagram->name[0] != 0)
	{
		crt_addTitle(chart, chart->diagram->name, chart->diagram->nameSize, 0);
	}


	xlsx_tagL(xml, "c:autoTitleDeleted");
	xlsx_AttrString(xml, "val", "0");
	xlsx_endL(xml);

	xlsx_tag(xml, "c:plotArea");
	xlsx_tagL(xml, "c:layout");
	xlsx_endL(xml);

	switch (chart->diagram->typeMain)
	{
	case CHART_LINEAR:
	{
		crt_addLineChart(chart, chart->xAxis, chart->yAxis->id, chart->seriesSet, 0);
		break;
	}
	case CHART_BAR:
	{
		crt_addBarChart(chart, chart->xAxis, chart->yAxis->id, chart->seriesSet, 0,
			chart->diagram->barDir, chart->diagram->barGroup);
		break;
	}
	case CHART_SCATTER:
	{
		chart->xAxis->sourceLinked = 1;
		crt_addScatterChart(chart, chart->xAxis->id, chart->yAxis->id, chart->seriesSet, 0,
			chart->diagram->scatterStyle);
		break;
	}
	case CHART_NONE: return -1;
	}

	switch (chart->diagram->typeAdditional)
	{
	case CHART_LINEAR:
	{
		crt_addLineChart(chart, chart->xAxis, chart->yAxis->id, chart->seriesSetAdd, 0);
		break;
	}
	case CHART_BAR:
	{
		crt_addBarChart(chart, chart->x2Axis, chart->yAxis->id, chart->seriesSetAdd,
			chart->seriesSetAdd->top + 1, chart->diagram->barDir, chart->diagram->barGroup);
		break;
	}
	case CHART_SCATTER:
	{
		chart->xAxis->sourceLinked = -2;
		crt_addScatterChart(chart, chart->xAxis->id, chart->yAxis->id, chart->seriesSet,
			chart->seriesSet->top + 1, chart->diagram->scatterStyle);
		break;
	}
	case CHART_NONE: break;
		/*default:
		break;*/
	}

	switch (chart->diagram->typeMain)
	{
	case CHART_LINEAR:
	case CHART_BAR:
		crt_addXAxis(chart, chart->xAxis, chart->yAxis->id);
		crt_addYAxis(chart, chart->yAxis, chart->xAxis->id);
		break;
	case CHART_SCATTER:
		crt_addXAxis(chart, chart->xAxis, chart->yAxis->id); /// Here was painfull error!!!! E.N.
		crt_addYAxis(chart, chart->yAxis, chart->xAxis->id);
		break;
	case CHART_NONE: return -3;
	}

	if (!vec_empty(chart->seriesSetAdd))
	{
		switch (chart->diagram->typeAdditional)
		{
		case CHART_LINEAR:
		case CHART_BAR:
			crt_addXAxis(chart, chart->x2Axis, chart->y2Axis->id);
			crt_addYAxis(chart, chart->y2Axis, chart->x2Axis->id);
			break;
		case CHART_SCATTER:
			crt_addXAxis(chart, chart->x2Axis, chart->y2Axis->id);
			crt_addYAxis(chart, chart->y2Axis, chart->x2Axis->id);
			break;
		case CHART_NONE: return -4;
		}
	}

	crt_addTableData(chart);

	xlsx_end(xml, "c:plotArea");

	crt_addLegend(chart);

	xlsx_tagL(xml, "c:plotVisOnly");
	xlsx_AttrString(xml, "val", "1");
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:dispBlanksAs");
	xlsx_AttrString(xml, "val", "gap");
	xlsx_endL(xml);

	xlsx_tagL(xml, "c:showDLblsOverMax");
	xlsx_AttrString(xml, "val", "0");
	xlsx_endL(xml);

	xlsx_end(xml, "c:chart");
	xlsx_end(xml, "c:chartSpace");

	xlsx_freeWriter(&xml);

	xml = chart->xmlWriter = NULL;

	return 1;
}

void crt_cellRangeString(char retValBuf[], char* title, PCELL_COORD CellFrom, PCELL_COORD CellTo) {
	char tmpBufFrom[256] = "", tmpBufTo[256] = "";
	cell_coord_toString(tmpBufFrom, CellFrom);
	cell_coord_toString(tmpBufTo, CellTo);
	snprintf(retValBuf, 256, "\'%s\'!$%s:$%s", title, tmpBufFrom, tmpBufTo);
}
